 var db = connect("localhost:27017/ekaushalnsdc");
 db.smartreports.remove({})

 function  paymentDtlsReport(){  
//   db.smartreports.remove({'reportName': 'paymentDtlsReport'});   
    db.payments.find({}).forEach(pymntsData => {
    if (pymntsData && pymntsData.isComplete) {
        let resObj = {
            'reportName': 'paymentDtlsReport',
            'userName': pymntsData['userId'] ? pymntsData['userId'] : '',
            'dwnldInvoice' : '',
            'status' : '',
            'paymentBy' : '',
            'paymentId': '',
            'tranctnID' : '',
            'invoiceNo' : '',
            'tpAplnNo' : '',
            'tcAplnNo' : '',
            'tpName' : '',
            'tcName' : '',
            'tpId' : '',
            'trainingCntreType': '',
            'feeDscptn' : '',
            'sscName' : '',
            'qpCode' : '',
            'applnFees':  '',
            'jobRoleFees':  '',
            'convFees':  '',
            'totalAmount':'',
            'tranctnDate' : '',
            'tranctnRslt' : '',
            'address' : '',
            'state' : '',
            'district' : '',
            'paymentMode' : ''
        };
        if (pymntsData['responseMetadata']) {
            if(pymntsData['responseMetadata']['billing_name']){
                resObj['paymentBy'] = pymntsData['responseMetadata']['billing_name'];
            }else{
                resObj['paymentBy'] = '';
            }
            if (pymntsData['responseMetadata']['order_id']) {
                resObj['paymentId'] = pymntsData['responseMetadata']['order_id'];
            } else {
                resObj['paymentId'] = '';
            }
            if (pymntsData['responseMetadata']['tracking_id']) {
                resObj['tranctnID'] = pymntsData['responseMetadata']['tracking_id'];
            } else {
                resObj['tranctnID'] = '';
            }
            if (pymntsData['responseMetadata']['mer_amount']) {
                resObj['totalAmount'] = pymntsData['responseMetadata']['mer_amount'];
            } else {
                resObj['totalAmount'] = '';
            }
            if (pymntsData['responseMetadata']['trans_date']) {
                resObj['tranctnDate'] = pymntsData['responseMetadata']['trans_date'];
            } else {
                resObj['tranctnDate'] = '';
            }
            if (pymntsData['responseMetadata']['payment_mode']) {
                resObj['paymentMode'] = pymntsData['responseMetadata']['payment_mode'];
            } else {
                resObj['paymentMode'] = '';
            }
            if (pymntsData['responseMetadata']['order_status']) {
                resObj['tranctnRslt'] = pymntsData['responseMetadata']['order_status'];
            } else {
                resObj['tranctnRslt'] = '';
            }
            if (pymntsData['responseMetadata']['billing_address']) {
                resObj['address'] = pymntsData['responseMetadata']['billing_address'];
            } else {
                resObj['address'] = '';
            }
            if (pymntsData['responseMetadata']['billing_state']) {
                resObj['state'] = pymntsData['responseMetadata']['billing_state'];
            } else {
                resObj['state'] = '';
            }
            if (pymntsData['responseMetadata']['billing_city']) {
                resObj['district'] = pymntsData['responseMetadata']['billing_city'];
            } else {
                resObj['district'] = '';
            }
        }
        // Refund colletion fields
        resObj.merchantId = '';
        resObj.refundDate = '';           
        resObj.referenceNo = '';
        resObj.refundStatus = '';
        resObj.refundMode = '';
        resObj.refundBy = '';
        resObj.refundAmount = '';
        resObj.remarks = '';

        if (pymntsData['responseMetadata']['tracking_id']) {
            let refnData = db.refunds.find({'requestMetadata.reference_no': pymntsData['responseMetadata']['tracking_id']}).limit(1).toArray();
            if (refnData && refnData.length) {

                resObj.refundDate = refnData[0]['date'] ? refnData[0]['date'] : '';

                if(refnData[0]['requestMetadata']){
                    if (refnData[0]['requestMetadata']['merchant_id']) {
                        resObj['merchantId'] = refnData[0]['requestMetadata']['merchant_id'];
                    } else {
                        resObj['merchantId'] = '';
                    }
                    if (refnData[0]['requestMetadata']['subscriptionDetails'] && refnData[0]['requestMetadata']['subscriptionDetails']['role']) {
                        resObj['refundBy'] = refnData[0]['requestMetadata']['subscriptionDetails']['role'];
                    } else {
                        resObj['refundBy'] = '';
                    }
                }
                if (refnData[0]['responseMetadata']) {
                    if (refnData[0]['responseMetadata']['refund_ref_no']) {
                        resObj['referenceNo'] = refnData[0]['responseMetadata']['refund_ref_no'];
                    } else {
                        resObj['referenceNo'] = '';
                    }
                    if (refnData[0]['responseMetadata']['refund_request']) {
                        resObj['refundStatus'] = refnData[0]['responseMetadata']['refund_request'];
                    } else {
                        resObj['refundStatus'] = '';
                    }
                    if (refnData[0]['responseMetadata']['Refund_Order_Result'] && refnData[0]['responseMetadata']['Refund_Order_Result']['reason']) {
                        resObj['remarks'] = refnData[0]['responseMetadata']['Refund_Order_Result']['reason'];
                    } else {
                        resObj['remarks'] = '';
                    }
                    if (refnData[0]['responseMetadata']['refund_amount']) {
                        resObj['refundAmount'] = refnData[0]['responseMetadata']['refund_amount'];
                    } else {
                        resObj['refundAmount'] = '';
                    }
                }   
            }
        }
        // printjson(resObj);
       db.smartreports.insertOne(resObj);
    }
    
})
}

function tcRefundDtls() {
        
        db.trainingcentre.find({}).forEach(tc => {
            if (tc) {
                var tcObj = {
                    'reportName': 'tcRefunddReport',
                    'tcId': tc['userName'] ? tc['userName'] : '',
                    'tcName': tc['trainingCentreName'] ? tc['trainingCentreName'] : '',
                    'tcType' : tc['trainingCenterType']? tc['trainingCenterType']:'',
                    'acHolderNm': '',
                    'bankNm': '',
                    'branchNm' : '',
                    'bankAcNo' : '',
                    'ifscCode' : '',
                    'status' : '',
                    'action' : ''
                };
                // printjson(tcObj)
                db.smartreports.insertOne(tcObj);
            }
        })
    }


function PaymentFun(){
    paymentDtlsReport()
    tcRefundDtls()
}
PaymentFun()