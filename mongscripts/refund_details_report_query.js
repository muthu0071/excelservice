//Query for refund details report by Ragu Kumar

db.getCollection('refunds').aggregate([{
    "$project": {
        "reportName": "refundDtlsReport",
        "_id": 0,
        "merchantId": {
            "$ifNull": ["$requestMetadata.merchant_id", ""]
        },
        "trxnID": {
            "$ifNull": ["$requestMetadata.reference_no", ""]
        },
        "userID": {
            "$ifNull": ["$userId", ""]
        },
        "refndAmnt": {
            "$ifNull": ["$requestMetadata.refund_amount", ""]
        },
        "feeType": {
            "$ifNull": ["$requestMetadata.subscriptionDetails.referenceType", ""]
        },
        "trxnDate": {
            "$ifNull": ["$date", ""]
        },
        "trxnResult": {
            "$ifNull": ["$responseMetadata.refund_request", ""]
        }
    }
}, {
    "$lookup": {
        "from": "trainingcentre",
        "localField": "userID",
        "foreignField": "userName",
        "as": "tcData"
    }
}, {
    "$unwind":"$tcData"
}, {
    "$addFields": {
        "tpName": {
            "$ifNull": ["$tcData.trainingPartner.name", ""]
        },
        "tpID": {
            "$ifNull": ["$tcData.trainingPartner.userName", ""]
        },
        "tcID": {
            "$ifNull": ["$tcData.userName", ""]
        },
        "tcName": {
            "$ifNull": ["$tcData.trainingCentreName", ""]
        },
        "tcType": {
            "$ifNull": ["$tcData.trainingCenterType", ""]
        },
         "tcStatus":{
             "$ifNull": ["$tcData.status", ""]
        },
     }
},{
    "$project": {
        "tcData": 0,
         "userID" : 0,
    }
}])
