var db = connect("localhost:27017/ekaushalnsdc");
load('moment.js')
db.smartreports.remove({})

//Script for Total Counts Reports @Author RK
function totCountReports() {
    var tpReg = db.trainingpartner.find({}).toArray();
    var resObj = {
        'reportName': 'totCountReport',
        "totTpReg": tpReg.length,
        "totTcReg": 0,
        "totTpPayment":0,
        "tcPymntforAplnFee" : 0,
        "tcPymntforContiniousMonitoring Fee": 0,
        "tcPymntforRe-InspectionApplication Fee": 0,
        "tcPymntforAffiliationFee": 0,
        "numOfInspectionsAssignedtoIMACS": 0,
        "numOfInspectionsDonebyIMACS": 0,
        "numOfInspectionsAssignedtoQUACA": 0,
        "numOfInspectionsDoneByQUACA": 0,
        "tcAccreditedBySSC": 0,
        "tcConditionalAccreditedBySSC": 0,
        "tcNotAccreditedBySSC": 0,
        "numOfTCsAffiliated": ""
    }, tpDrCnt = 0, tpDnrCnt = 0, tcDrCnt = 0, tcDnrCnt = 0, tpgrpByDt = {}, tcgrpByDt = {};
    tpReg.forEach((pymtyp)=>{
        if(pymtyp.userName){
            var pymnttyp = db.payments.find({'userId':pymtyp.userName},{ "requestMetadata.subscriptionDetails.role":1}).limit(1).toArray();
            if(pymnttyp && pymnttyp[0] && pymnttyp[0].requestMetadata && pymnttyp[0].requestMetadata.subscriptionDetails && pymnttyp[0].requestMetadata.subscriptionDetails.role  == 'Training Partner'){
                resObj.totTpPayment ++;
            }
        }
        resObj.totTcReg ++;
        var createdOn = moment(pymtyp._id.getTimestamp()).format('YYYY-MM-DD');
        if(tpgrpByDt[createdOn]){
            tpgrpByDt[createdOn]++
        }else{
            tpgrpByDt[createdOn] = 1
        }
        if(pymtyp.status == 'approved' || pymtyp.status == 'DAAPPROVED'){
            tpDrCnt += 1 ;
        }else if(pymtyp.status == 'DAREJECTED'){
            tpDnrCnt += 1;
        }
    })
    db.trainingcentre.find({}).toArray().forEach((tcData)=>{
        resObj.totTcReg ++;
        var createdOn = moment(tcData._id.getTimestamp()).format('YYYY-MM-DD');
        if(tcgrpByDt[createdOn]){
            tcgrpByDt[createdOn]++
        }else{
            tcgrpByDt[createdOn] = 1
        }
        if(tcData.status == 'approved' || tcData.status == 'DAAPPROVED'){
            tcDrCnt += 1 ;
        }else if(tcData.status == 'DAREJECTED'){
            tcDnrCnt += 1;
        }
        if(tcData.userName){
            var tcWrkFlw = db.tcworkflow.find({'tcId':tcData.userName},{ 'zone': 1,'actionTakenBy': 1,'assignedNextUser': 1,'actionTakenByRole':1, "status":1}).limit(1).toArray();
            if(tcWrkFlw && tcWrkFlw[0]){
                if(tcWrkFlw[0].actionTakenByRole && tcWrkFlw[0].actionTakenByRole == 'SSC'){
                    if(tcWrkFlw[0].status){
                        if(tcWrkFlw[0].status == "Not Accrediated"){
                            resObj.tcNotAccreditedBySSC ++;
                        }
                        if(tcWrkFlw[0].status == "Accrediated"){
                            resObj.tcAccreditedBySSC ++;
                        }
                        if(tcWrkFlw[0].status ==  "Conditionally Accrediated"){
                            resObj.tcConditionalAccreditedBySSC ++;
                        }
                    }
                }
                if(tcWrkFlw[0].zone && tcWrkFlw[0].zone == "QACA"){
                    if(tcWrkFlw[0].actionTakenBy){
                        resObj.numOfInspectionsDoneByQUACA ++;
                    }
                    if(tcWrkFlw[0].assignedNextUser){
                        resObj.numOfInspectionsAssignedtoQUACA ++;
                    }
                }else if(tcWrkFlw[0].zone  && tcWrkFlw[0].zone == "IMAC"){
                    if(tcWrkFlw[0].actionTakenBy){
                        resObj.numOfInspectionsDonebyIMACS ++;
                    }
                    if(tcWrkFlw[0].assignedNextUser){
                        resObj.numOfInspectionsAssignedtoIMACS ++;
                    }
                }
            }
        }
    });
    resObj.tpDrCnt = tpDrCnt;
    resObj.tpDnrCnt = tpDnrCnt;
    resObj.tcDrCnt = tcDrCnt;
    resObj.tcDnrCnt = tcDnrCnt;
    resObj.tpgrpByDt = tpgrpByDt;
    resObj.tcgrpByDt = tcgrpByDt;
    // printjson(resObj)
    db.smartreports.insertOne(resObj);
}

function whenConnected() {
    totCountReports();
}

whenConnected();