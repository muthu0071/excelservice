var db = connect("localhost:27017/ekaushalnsdc");
db.smartreports.remove({})
// Script for TP Reports @Author Senthur Mahadevn
function tpReports() {
    db.trainingpartner.aggregate([{
            "$project": {
                "userName": 1,
                "spoc": 1,
                "firstName": 1,
                "address": 1,
                "status": 1,
                "schemes.schemeReferenceId": 1
            }
        },
        {
            "$facet": {
                "withscheme": [{
                        "$match": {
                            "schemes": {
                                "$exists": true
                            }
                        }
                    },
                    {
                        "$unwind": "$schemes"
                    },
                    {
                        "$group": {
                            "_id": "$userName",
                            "userName": {
                                "$first": "$userName"
                            },
                            "spoc": {
                                "$first": "$spoc"
                            },
                            "firstName": {
                                "$first": "$firstName"
                            },
                            "address": {
                                "$first": "$address"
                            },
                            "status": {
                                "$first": "$status"
                            },
                            "schemes": {
                                "$addToSet": "$schemes.schemeReferenceId"
                            }
                        }
                    },
                    {
                        "$unwind": "$schemes"
                    },
                    {
                        "$lookup": {
                            "from": "scheme",
                            "let": {
                                "schemeid": "$schemes"
                            },
                            "pipeline": [{
                                    "$match": {
                                        "$expr": {
                                            "$and": {
                                                "$eq": ["$schemeId", "$$schemeid"]
                                            }
                                        }
                                    }
                                },
                                {
                                    "$project": {
                                        "schemeDetails.creationDetails.schemeName": 1,
                                        "_id": 0
                                    }
                                }
                            ],
                            "as": "schemedata"
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$schemedata",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    {
                        "$project": {
                            "tpId": "$userName",
                            "tpName": "$firstName",
                            "tpSpocMob": "$spoc.mobileNumber",
                            "tpSpocEmail": "$spoc.email",
                            "state": "$address.state",
                            "status": 1,
                            "schemeName": "$schemedata.schemeDetails.creationDetails.schemeName"
                        }
                    },
                    {
                        "$group": {
                            "_id": "$tpId",
                            "tpId": {
                                "$first": "$tpId"
                            },
                            "tpName": {
                                "$first": "$tpName"
                            },
                            "tpSpocMob": {
                                "$first": "$tpSpocMob"
                            },
                            "status": {
                                "$first": "$status"
                            },
                            "tpSpocEmail": {
                                "$first": "$tpSpocEmail"
                            },
                            "state": {
                                "$first": "$state"
                            },
                            "schemes": {
                                "$addToSet": "$schemeName"
                            }
                        }
                    }
                ],
                "withoutscheme": [{
                        "$match": {
                            "schemes": {
                                "$exists": false
                            }
                        }
                    },
                    {
                        "$project": {
                            "tpId": "$userName",
                            "tpName": "$firstName",
                            "tpSpocMob": "$spoc.mobileNumber",
                            "tpSpocEmail": "$spoc.email",
                            "state": "$address.state",
                            "status": 1,
                        }
                    }
                ]
            }
        },
        {
            "$project": {
                "tp": {
                    "$concatArrays": ["$withscheme", "$withoutscheme"]
                }
            }
        },
        {
            "$unwind": "$tp"
        },
        {
            "$replaceRoot": {
                "newRoot": "$tp"
            }
        },
        {
            "$addFields": {
                "reportName": "tpReport"
            }
        }
    ]).forEach(tp => {
        if (tp) {
            let tpObj = {
                'reportName': tp['reportName'] ? tp['reportName'] : '',
                'tpId': tp['tpId'] ? tp['tpId'] : '',
                'tpName': tp['tpName'] ? tp['tpName'] : '',
                'tpSpocMob': tp['tpSpocMob'] ? tp['tpSpocMob'] : '',
                'tpSpocEmail': tp['tpSpocEmail'] ? tp['tpSpocEmail'] : '',
                'state': tp['state'] ? tp['state'] : '',
                'daStatus': tp['status'] ? tp['status'] : '',
                'schemes': tp['schemes'] ? tp['schemes'] : [],
                'allowTc':'',
                'createdTc':'',
                'pendingTc':'',
                'recomendedBy':''
            };
            db.smartreports.insertOne(tpObj);
        }
    });
}

//Script for Recomendation Report @Author SenthurMahadevan
function recomendationReports() {
    db.trainingcentre.find({    
        // userName: "TC_003885"
    }).forEach(tc => {
        if (tc) {
            var tcObj = {
                "reportName": "recomendationReport",
                "tcId": tc["userName"] ? tc["userName"] : '',
                "tcName": tc["trainingCentreName"] ? tc["trainingCentreName"] : '',
                "tcId": tc["userName"] ? tc["userName"] : '',
                "tcSubmissionDt": tc["submittedOn"] ? tc["submittedOn"] : '',
                "tcType": tc["trainingCenterType"] ? tc["trainingCenterType"] : '',
                "tcStatus": tc["status"] ? tc["status"] : '',
                "DAreport": tc["DAreport"] ? tc["DAreport"] : ''

            };
            if (tc["spoc"]) {
                if (tc["spoc"]["firstName"]) {
                    tcObj["tcSpocName"] = tc["spoc"]["firstName"];
                } else {
                    tcObj["tcSpocName"] = "";
                }
                if (tc["spoc"]["mobileNumber"]) {
                    tcObj["tcSpocMobile"] = tc["spoc"]["mobileNumber"];
                } else {
                    tcObj["tcSpocMobile"] = "";
                }
                if (tc["spoc"]["email"]) {
                    tcObj["tcSpocEMail"] = tc["spoc"]["email"];
                } else {
                    tcObj["tcSpocEMail"] = "";
                }
            } else {
                tcObj["tcSpocName"] = "";
                tcObj["tcSpocMobile"] = "";
                tcObj["tcSpocEMail"] = "";

            }

            if (tc["address"]) {
                if (tc["address"]["state"] && tc["address"]["state"]["id"]) {
                    tcObj["tcStateId"] = tc["address"]["state"]["id"];
                } else {
                    tcObj["tcStateId"] = "";
                }
                if (tc["address"]["state"] && tc["address"]["state"]["name"]) {
                    tcObj["tcStateName"] = tc["address"]["state"]["name"];
                } else {
                    tcObj["tcStateName"] = "";
                }
                if (tc["address"]["district"] && tc["address"]["district"]["name"]) {
                    tcObj["tcDistName"] = tc["address"]["district"]["name"];
                } else {
                    tcObj["tcDistName"] = "";
                }
                if (tc["address"]["parliamentaryConstituency"] && tc["address"]["parliamentaryConstituency"]["name"]) {
                    tcObj["tcConstituencyName"] = tc["address"]["parliamentaryConstituency"]["name"];
                } else {
                    tcObj["tcConstituencyName"] = "";
                }
                if (tc["address"]["address1"] && tc["address"]["address1"]) {
                    tcObj["tcFullAddrss"] = tc["address"]["address1"];
                } else {
                    tcObj["tcFullAddrss"] = "";
                }
                if (tc["address"]["pincode"] && tc["address"]["pincode"]) {
                    tcObj["tcPincode"] = tc["address"]["pincode"];
                } else {
                    tcObj["tcPincode"] = "";
                }
            } else {
                tcObj["tcStateId"] = "";
                tcObj["tcStateName"] = "";
                tcObj["tcDistName"] = "";
                tcObj["tcConstituencyName"] = "";
                tcObj["tcFullAddrss"] = "";
                tcObj["tcPincode"] = "";
            }
            tcObj["daDate"] = "";
            if (tc["daReviews"] && tc["daReviews"].length) {
                var daData = {};
                daData = tc["daReviews"].pop();
                if (daData && daData["date"]) {
                    tcObj["daDate"] = daData["date"]
                }
            }

            tcObj["qp"] = "";
            tcObj["jobRole"] = "";
            if (tc["jobRoles"] && tc["jobRoles"].length) {
                for (var i = tc["jobRoles"].length - 1; i >= 0; i--) {
                    if (tc["jobRoles"][i]["qp"]) {
                        tcObj["qp"] += tc["jobRoles"][i]["qp"];
                    }
                    if (tc["jobRoles"][i]["name"]) {
                        tcObj["jobRole"] += tc["jobRoles"][i]["name"];

                    }
                    if (i !== 0) {
                        if (tc["jobRoles"][i]["qp"]) {
                            tcObj["qp"] += ',';

                        }
                        if (tc["jobRoles"][i]["name"]) {
                            tcObj["jobRole"] += ',';
                        }
                    }
                }
            }
            tcObj["tpId"] = "";
            tcObj["tpSpocName"] = "";
            tcObj["tpSpocEmail"] = "";
            tcObj["tpSpocMobile"] = "";
            tcObj["tpName"] = "";
            if (tc["trainingPartner"] && tc["trainingPartner"]["userName"]) {
                db.trainingpartner.find({
                    "userName": tc["trainingPartner"]["userName"]
                }, {
                    "userName": 1,
                    "firstName": 1,
                    "spoc": 1
                }).limit(1).toArray().forEach(function (tpData) {
                    if (tpData) {
                        tcObj["tpId"] = tpData["userName"];
                        tcObj["tpName"] = tpData["firstName"];
                        if (tpData["spoc"] && tpData["spoc"]["firstName"]) {
                            tcObj["tpSpocName"] = tpData["spoc"]["firstName"];
                        }
                        if (tpData["spoc"] && tpData["spoc"]["email"]) {
                            tcObj["tpSpocEmail"] = tpData["spoc"]["email"];
                        }

                        if (tpData["spoc"] && tpData["spoc"]["mobileNumber"]) {
                            tcObj["tpSpocMobile"] = `${tpData["spoc"]["mobileNumber"]}`;
                        }

                    }
                })

            }
            db.smartreports.insertOne(tcObj);

        }
    })
}

// Script for TP status Reports
function tcStatusReports() {
    db.trainingcentre.find({}).forEach(tc_data => {
        let tc_dataObj = {
            'reportName': 'tcStatusReport'
        };
        if (tc_data) {
            //TP ID
            if (tc_data["trainingPartner"] && tc_data["trainingPartner"]["userName"]) {
                tc_dataObj['trainingPartneruserName'] = tc_data["trainingPartner"]["userName"];
            } else {
                tc_dataObj['trainingPartneruserName'] = ''
            }

            //TP NAME
            if (tc_data["trainingPartner"] && tc_data["trainingPartner"]["name"]) {
                tc_dataObj['trainingPartnername'] = tc_data["trainingPartner"]["name"];
            } else {
                tc_dataObj['trainingPartnername'] = ''
            }

            //TC ID
            if (tc_data["userName"]) {
                tc_dataObj['trainingcentreuserName'] = tc_data["userName"];
            } else {
                tc_dataObj['trainingcentreuserName'] = ''
            }

            //TC creatION DATA
            if (tc_data["createdOn"]) {
                tc_dataObj["createdOn"] = tc_data["createdOn"];

            } else {
                tc_dataObj['createdOn'] = ''
            }

            //TC NAME
            if (tc_data["trainingCentreName"]) {
                tc_dataObj['trainingCentreName'] = tc_data["trainingCentreName"];
            } else {
                tc_dataObj['trainingCentreName'] = ''
            }

            //TC SPOC MOBILE
            if (tc_data["spoc"] && tc_data["spoc"]["mobileNumber"]) {
                tc_dataObj['spocmobileNumber'] = tc_data["spoc"]["mobileNumber"];
            } else {
                tc_dataObj['spocmobileNumber'] = ''
            }

            //TC SPOC EMAIL ID
            if (tc_data["spoc"] && tc_data["spoc"]["email"]) {
                tc_dataObj['spocemail'] = tc_data["spoc"]["email"];
            } else {
                tc_dataObj['spocemail'] = ''
            }

            //SCHEME
            if (tc_data["scheme"]) {
                tc_dataObj['scheme'] = tc_data["scheme"];
            } else {
                tc_dataObj['scheme'] = ''
            }

            //STATE
            if (tc_data["address"] && tc_data["address"]["state"] && tc_data["address"]["state"]["name"]) {
                tc_dataObj['addressstatename'] = tc_data["address"]["state"]["name"];
            } else {
                tc_dataObj['addressstatename'] = ''
            }

            //DISTRICT
            if (tc_data["address"] && tc_data["address"]["district"] && tc_data["address"]["district"]["name"]) {
                tc_dataObj['addressdistrictname'] = tc_data["address"]["district"]["name"];
            } else {
                tc_dataObj['addressdistrictname'] = ''
            }

            //CENTER STATUS
            if (tc_data["status"]) {
                tc_dataObj['status'] = tc_data["status"];
            } else {
                tc_dataObj['status'] = ''
            }

            //DISTRTC CAAF APPROVAL STATUS
            if (tc_data["tcstatus"]) {
                tc_dataObj['tcstatus'] = tc_data["tcstatus"];
            } else {
                tc_dataObj['tcstatus'] = ''
            }
            db.smartreports.insertOne(tc_dataObj);
        }
    });
}
//Script for Target Allocation Report
function targetAlloctnReports() {
    db.trainingcentre.find({}).forEach(tc => {
        if (tc) {
            var tcObj = {
                'reportName': 'targetAllocationReport',
                'tcUserName': tc['userName'] ? tc['userName'] : '',
                'tcName': tc['trainingCentreName'] ? tc['trainingCentreName'] : '',
                'trainingCenterType' : tc['trainingCenterType']? tc['trainingCenterType']:'',
                'tcSpocName': '',
                'tcSpocMobile': '',
                'stateName' : '',
                'districtName' : '',
                'constituencyName' : '',
                'address' : '',
                'pincode' : '',
                'tpSpocName': '',
                'tpSpocEmail': '',
                'tpSpocMobile': '',
                'tpName': '',
                "tcSpocEMail" : ""
            };
            if (tc['spoc']) {
                if (tc['spoc']['firstName']) {
                    tcObj['tcSpocName'] = tc['spoc']['firstName'];
                } else {
                    tcObj['tcSpocName'] = '';
                }
                if (tc['spoc']['mobileNumber']) {
                    tcObj['tcSpocMobile'] = tc['spoc']['mobileNumber'];
                } else {
                    tcObj['tcSpocMobile'] = '';
                }
                if (tc['spoc']['email']) {
                    tcObj['tcSpocEMail'] = tc['spoc']['email'];
                } else {
                    tcObj['tcSpocEMail'] = '';
                }
            }
            if (tc['address']) {
                if (tc['address']['state'] && tc['address']['state']['name']) {
                    tcObj['stateName'] = tc['address']['state']['name'];
                } else {
                    tcObj['stateName'] = '';
                }
                if (tc['address']['district'] && tc['address']['district']['name']) {
                    tcObj['districtName'] = tc['address']['district']['name'];
                } else {
                    tcObj['districtName'] = '';
                }
                if (tc['address']['parliamentaryConstituency'] && tc['address']['parliamentaryConstituency']['name']) {
                    tcObj['constituencyName'] = tc['address']['parliamentaryConstituency']['name'];
                } else {
                    tcObj['constituencyName'] = '';
                }
                if (tc['address']['address1'] && tc['address']['address1']) {
                    tcObj['address'] = tc['address']['address1'];
                } else {
                    tcObj['address'] = '';
                }
                if (tc['address']['pincode'] && tc['address']['pincode']) {
                    tcObj['pincode'] = tc['address']['pincode'];
                } else {
                    tcObj['pincode'] = '';
                }
            } 

            if (tc['jobRoles'] && tc['jobRoles'].length) {
                tcObj.jobRolNm = '';
                for (var i = 0; i < tc['jobRoles'].length; i++) {
                    tcObj.jobRolNm += tc['jobRoles'][i].name +","
                }
            } else {
                tcObj.jobRolNm = '';
            }

            if (tc['trainingPartner'] && tc['trainingPartner']['userName']) {
                db.trainingpartner.find({
                    'userName': tc['trainingPartner']['userName']
                }, {
                    'userName': 1,
                    'firstName': 1,
                    'spoc': 1
                }).limit(1).toArray().forEach((tpData)=> {
                    if (tpData) {
                        tcObj['tpuserName'] = tpData['userName'];
                        tcObj['tpName'] = tpData['firstName'];
                        if (tpData['spoc'] && tpData['spoc']['firstName']) {
                            tcObj['tpSpocName'] = tpData['spoc']['firstName'];
                        }
                        if (tpData['spoc'] && tpData['spoc']['mobileNumber']) {
                            tcObj['tpSpocMobile'] = tpData['spoc']['mobileNumber'];
                        }
                        // }
                        if (tpData['spoc'] && tpData['spoc']['email']) {
                            tcObj['tpSpocEmail'] = tpData['spoc']['email'];
                        }
                        // }
                    }
                })
                // })
            }
            // printjson(tcObj)
            db.smartreports.insertOne(tcObj);
        }
    })

}
// Script for Continuous Monitoring Status Report..
function continuousMonitorReport() {
    db.trainingcentre.find({}).forEach(tc_data => {
        if (tc_data) {
            let tcWrkFlow = [];
            let IAData = [];
            let tcObj = {
                'reportName': 'continuousReport',
                'tcId': tc_data['userName'] ? tc_data['userName'] : '',
                'tcName': tc_data['trainingCentreName'] ? tc_data['trainingCentreName'] : ''

            };
            if (tc_data['userName']) {
                //    tcWrkFlow = db.tcworkflow.find({"tcId" : tc_data['userName']}).sort({_id : -1}).limit(1).toArray();
                tcWrkFlow = db.tcworkflow.find({
                    "tcId": tc_data['userName'],
                    "assignedNextUserRole": "Desktop Assessor"
                }).sort({
                    _id: -1
                }).limit(1).toArray();
            };

            if (tc_data['trainingPartner'] && tc_data['trainingPartner']['userName']) {
                tcObj.tpId = tc_data['trainingPartner']['userName'];
            } else {
                tcObj.tpId = " ";
            };

            if (tcWrkFlow && tcWrkFlow.length && tcWrkFlow[0].createdOn) {
                tcObj.DAAsingndOn = tcWrkFlow[0].createdOn;
            } else {
                tcObj.DAAsingndOn = " ";
            };

            if (tcWrkFlow && tcWrkFlow.length && tcWrkFlow[0].actionTakenOn) {
                tcObj.DADoneOn = tcWrkFlow[0].actionTakenOn;
            } else {
                tcObj.DADoneOn = " ";
            };

            if (tcWrkFlow && tcWrkFlow.length && tcWrkFlow[0].assignedNextUserRole) {
                tcObj.DADoneBy = tcWrkFlow[0].assignedNextUserRole;
            } else {
                tcObj.DADoneBy = " ";
            };

            if (tc_data['daReviews'] && tc_data['daReviews'].length && tc_data['daReviews'][tc_data['daReviews'].length - 1].userName) {
                tcObj.DAAgency = tc_data['daReviews'][tc_data['daReviews'].length - 1].userName;
            } else {
                tcObj.DAAgency = '';
            };

            if (tc_data['daReviews'] && tc_data['daReviews'].length && tc_data['daReviews'][tc_data['daReviews'].length - 1]) {
                if (tc_data['daReviews'][tc_data['daReviews'].length - 1].review && tc_data['daReviews'][tc_data['daReviews'].length - 1].review.finalComment) {
                    tcObj.remark = tc_data['daReviews'][tc_data['daReviews'].length - 1].review.finalComment;
                } else {
                    tcObj.remark = " ";
                }
            } else {
                tcObj.remark = " ";
            };

            db.smartreports.insertOne(tcObj);
        };
    });
}
// Script for Agency Performance Report
function agencyPerformanceReport() {
    db.trainingcentre.find({}).forEach(tcData => {
        if (tcData) {
            let tc_obj = {
                'reportName': 'agencyPerformanceReport',
                'tcId': tcData['userName'] ? tcData['userName'] : '',
                'tcName': tcData['trainingCentreName'] ? tcData['trainingCentreName'] : '',
                'tcType': tcData['type'] ? tcData['type'] : ''
            };
            let tcWtkFlwData = [];
            if (tcData['userName']) {
                tcWtkFlwData = db.tcworkflow.find({
                    "tcId": tcData['userName'],
                    "assignedNextUserRole": "Inspection Agency"
                }).sort({
                    _id: -1
                }).limit(1).toArray();
            };

            if (tcData['trainingPartner'] && tcData['trainingPartner']['userName']) {
                tc_obj.tpId = tcData['trainingPartner']['userName'];
            } else {
                tc_obj.tpId = " ";
            };

            if (tcData['trainingPartner'] && tcData['trainingPartner']['name']) {
                tc_obj.tpName = tcData['trainingPartner']['name'];
            } else {
                tc_obj.tpName = ' ';
            };

            if (tcData['address'] && tcData['address']['addressLine']) {
                tc_obj.tcAdress = tcData['address']['addressLine'];
            } else {
                tc_obj.tcAdress = ' ';
            };

            if (tcData['address'] && tcData['address']['district'] && tcData['address']['district']['name']) {
                tc_obj.tcDist = tcData['address']['district']['name'];
            } else {
                tc_obj.tcDist = ' ';
            };

            if (tcData['address'] && tcData['address']['state'] && tcData['address']['state']['name']) {
                tc_obj.tcStat = tcData['address']['state']['name'];
            } else {
                tc_obj.tcStat = ' ';
            };

            if (tcData['submittedOn']) {
                tc_obj.submissionDat = tcData['submittedOn'];
            } else {
                tc_obj.submissionDat = ' ';
            };

            if (tcData['canReInspect']) {
                tc_obj.reInspctn = tcData['canReInspect'];
            } else {
                tc_obj.reInspctn = ' ';
            };

            if (tcData['daReviews'] && tcData['daReviews'].length && tcData['daReviews'][tcData['daReviews'].length - 1].date) {
                tc_obj.desktopAssmntDate = tcData['daReviews'][tcData['daReviews'].length - 1].date;
            } else {
                tc_obj.desktopAssmntDate = '';
            };

            if (tcData['daReviews'] && tcData['daReviews'].length && tcData['daReviews'][tcData['daReviews'].length - 1]['review'] &&
                tcData['daReviews'][tcData['daReviews'].length - 1]['review']['status']) {
                tc_obj.desktopAssmntStatus = tcData['daReviews'][tcData['daReviews'].length - 1]['review']['status'];
            } else {
                tc_obj.desktopAssmntStatus = '';
            };

            tc_obj.withdrawal = '';

            if (tcData['ActiveStatus']) {
                tc_obj.actStatus = tcData['ActiveStatus'];
            } else {
                tc_obj.actStatus = ' ';
            };

            if (tcData['daReviews'] && tcData['daReviews'].length && tcData['daReviews'][tcData['daReviews'].length - 1].userName) {
                tc_obj.DAAgencyNm = tcData['daReviews'][tcData['daReviews'].length - 1].userName;
            } else {
                tc_obj.DAAgencyNm = '';
            };

            if (tcWtkFlwData && tcWtkFlwData.length && tcWtkFlwData[0].spoc && tcWtkFlwData[0].spoc.firstName) {
                tc_obj.insAgenNm = tcWtkFlwData[0].spoc.firstName;
            } else {
                tc_obj.insAgenNm = '';
                
            };
            if (tcWtkFlwData && tcWtkFlwData.length && tcWtkFlwData[0].actionTakenOn) {
                tc_obj.insptnDate = tcWtkFlwData[0].actionTakenOn;
            } else {
                tc_obj.insptnDate = '';
            };

            if (tcData['qcReviews'] && tcData['qcReviews'].length && tcData['qcReviews'][tcData['qcReviews'].length - 1].date) {
                tc_obj.qcDate = tcData['qcReviews'][tcData['qcReviews'].length - 1].date;
            } else {
                tc_obj.qcDate = '';
            }

            tc_obj.SSCReviwDate = '';

            if (tcData['counter'] && tcData['counter']['jobroles']) {
                tc_obj.totNumOfJobRol = tcData['counter']['jobroles'];
            } else {
                tc_obj.totNumOfJobRol = ' ';
            };

            tc_obj.totNmOfJobRolRecmdByQA = '';
            tc_obj.totNmOfJobRolRecmdBySSC = '';

            if (tcData['jobRoles'] && tcData['jobRoles'].length) {
                tc_obj.jobRolNam = '';
                for (let i = 0; i < tcData['jobRoles'].length; i++) {
                    if(!tcData['jobRoles'][i].name){

                        tc_obj.jobRolNam += tcData['jobRoles'][i].name + ","
                    }
                };
            } else {
                tc_obj.jobRolNam = '';
            };

            tc_obj.qaStatus = '';
            tc_obj.sscStatus = '';

            db.smartreports.insertOne(tc_obj);
        };
    });
}



function whenConnected() {
    load("weeklyreports.js")
    print("tpReports")
    tpReports();//-->done 
    print("recomendationReports")
    recomendationReports();//pending
    print("tcStatusReports")
    tcStatusReports();//-->done
    print("targetAlloctnReports")
    targetAlloctnReports(); // --> script pending
    print("continuousMonitorReport")
    continuousMonitorReport(); //--> done
     print("--agencyPerformanceReport")
    agencyPerformanceReport(); //--> done

}
whenConnected();