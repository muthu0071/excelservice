function TP_TC_StatusReport() {
    var db = connect("localhost:27017/ekaushalnsdc");
    db.trainingcentre.find({}).forEach(tcData => {
        if (tcData) {
            let tcWrkFlw = [];
            let tc_obj = {
                'reportName': 'TP_TC_StatusReport',
                'tcId' : tcData['userName'] ? tcData['userName'] : '',
                'tcName' : tcData['trainingCentreName'] ? tcData['trainingCentreName'] : '',
                'tcCreatnDate' : tcData['createdOn'] ? tcData['createdOn'] : '',
                'centrStatus' : tcData['tcstatus'] ? tcData['tcstatus'] : '',
                'aplnNum' : tcData['applicationNumber'] ? tcData['applicationNumber'] : '',
                'scheme' : tcData['scheme'] ? tcData['scheme'] : ''
            };

            if (tcData['userName']) {
                tcWrkFlw = db.tcworkflow.find({"tcId" : tcData['userName'],"assignedNextUserRole":"Centre Inspector"}).sort({_id : -1}).limit(1).toArray();
            };

            if (tcData['trainingPartner'] && tcData['trainingPartner']['userName']) {
                tc_obj.tpId = tcData['trainingPartner']['userName'];
            } else {
                tc_obj.tpId = "";
            };

            if (tcData['trainingPartner'] && tcData['trainingPartner']['name']) {
                tc_obj.tpName = tcData['trainingPartner']['name'];
            } else {
                tc_obj.tpName = '';
            };

            if (tcData['spoc'] && tcData['spoc']['mobileNumber']) {
                tc_obj.TC_spocMble = tcData['spoc']['mobileNumber']
            } else {
                tc_obj.TC_spocMble = "";
            };

            if (tcData['spoc'] && tcData['spoc']['email']) {
                tc_obj.TC_spocEmail = tcData['spoc']['email'];
            } else {
                tc_obj.TC_spocEmail = "";
            };

            if (tcData['address'] && tcData['address']['state'] && tcData['address']['state']['name']) {
                tc_obj.tcStat = tcData['address']['state']['name'];
            } else {
                tc_obj.tcStat = '';
            };

            if (tcData['address'] && tcData['address']['district'] && tcData['address']['district']['name']) {
                tc_obj.tcDist = tcData['address']['district']['name'];
            } else {
                tc_obj.tcDist = '';
            };

            if (tcData['daReviews'] && tcData['daReviews'].length && tcData['daReviews'][tcData['daReviews'].length - 1].date) {
                tc_obj.DA_Date = tcData['daReviews'][tcData['daReviews'].length - 1].date;
            } else {
                tc_obj.DA_Date = '';
            };

            if (tcData['daReviews'] && tcData['daReviews'].length && tcData['daReviews'][tcData['daReviews'].length - 1]['review'] 
            && tcData['daReviews'][tcData['daReviews'].length - 1]['review']['status']) {
                tc_obj.DA_Status = tcData['daReviews'][tcData['daReviews'].length - 1]['review']['status'];
            } else {
                tc_obj.DA_Status = '';
            };

            if (tcData['inspectionCenterDates'] && tcData['inspectionCenterDates'].length 
            && tcData['inspectionCenterDates'][tcData['inspectionCenterDates'].length - 1].proposeddate) {
                tc_obj.inspectionStatusDate = tcData['inspectionCenterDates'][tcData['inspectionCenterDates'].length - 1].proposeddate;
            } else {
                tc_obj.inspectionStatusDate = "";
            }

            if (tcWrkFlw && tcWrkFlw['otherInformation'] && tcWrkFlw['otherInformation']['centreinspection'] 
            && tcWrkFlw['otherInformation']['centreinspection']['proposeddate']) {
                tc_obj.reInspctnDate = tcWrkFlw['otherInformation']['centreinspection']['proposeddate'];
            } else {
                tc_obj.reInspctnDate = "";
            };

            if (tcData['jobRoles'] && tcData['jobRoles'].length) {
                let jbRoleStatus = "";
                let jbRole = "";
                let qpCode = "";
                for (let i = 0; i < tcData['jobRoles'].length; i++) {
                    if (tcData['jobRoles'][i]['status']) {
                        jbRoleStatus += tcData['jobRoles'][i]['status'] + ","
                    };

                    if (tcData['jobRoles'][i]['name']) {
                        jbRole += tcData['jobRoles'][i]['name'] + ","
                    };

                    if (tcData['jobRoles'][i]['qp']) {
                        qpCode += tcData['jobRoles'][i]['qp'] + ","
                    };

                };
                tc_obj.jbRoleStatus = jbRoleStatus;
                tc_obj.jbRole = jbRole;
                tc_obj.qpCode = qpCode;
            } else {
                tc_obj.jbRoleStatus = "";
                tc_obj.jbRole = "";
                tc_obj.qpCode = "";
            };
            
            
            if (tcData['canReInspect']) {
                tc_obj.reInspctnStatus = tcData['canReInspect'];
            } else {
                tc_obj.reInspctnStatus = '';
            };

            if (tcData['trainingCenterType']) {
                if (tcData['trainingCenterType'] == 'Government') {
                    tc_obj.Govt_TC = 'Yes';
                } else {
                    tc_obj.Govt_TC = 'No';
                };
            } else {
                tc_obj.Govt_TC = '';
            };

            tc_obj.taggedBy = "";
            tc_obj.TcCAAFAprvlStatus = "";

            db.smartreports.insertOne(tc_obj);
        };
    });
}

TP_TC_StatusReport()
print("tpTcStatuReportover")