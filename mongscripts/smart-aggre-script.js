// Script for Continuous Monitoring Status Report..
db.trainingcentre.aggregate(
    [
         {
             $project : {
                 'reportName': 'continuousReport',
                 "tcId" : {
                     $ifNull : ["$userName",""]
                 },
                 "tcName" : {
                     $ifNull : ["$trainingCentreName",""]
                 },
                 "tpId" : {
                     $ifNull : ["$trainingPartner.userName",""]
                 },
                 "userName": {
                     "$ifNull": ["$userName", ""]
                 },
                 "daReviews" : {
                     $arrayElemAt : ["$daReviews",-1]
                 },
                 "_id" : 0
             }
         },
         {
            "$lookup" : {
                "from" : "tcworkflow",
                "let" : {"username" : "$userName"},            
                "pipeline" : [{
                        "$match" : {
                             "$expr" : {
                                 "$and" : [
                                     {"$eq" : ["$tcId", "$$username"]},
                                     {"$eq" : ["$assignedNextUserRole","Desktop Assessor"]}
                                 ]
                              }
                         }
                },
                {"$sort" : {"_id" : -1}},
                {"$limit" : 1},
                {"$project" : {
                                 "DAAsingndOn" : "$createdOn",
                                 "DADoneOn" : "$actionTakenOn",
                                 "DADoneBy" :"$assignedNextUserRole"
                              }
                }
                ],
                 "as" : "tcWrkFlw"
            } 
         },
         {
                 "$unwind" : {
                         "path" : "$tcWrkFlw",
                         "preserveNullAndEmptyArrays": true
                  }
         },
         {
                 "$addFields" : {
                         "DAAgency" : {$ifNull : ["$daReviews.userName",""]},
                         "remark" : {$ifNull : ["$daReviews.review.finalComment",""]},
                         "DAAsingndOn" : {$ifNull : ["$tcWrkFlw.DAAsingndOn", ""]},
                         "DADoneOn" : {$ifNull : ["$tcWrkFlw.DADoneOn",""]},
                         "DADoneBy" : {$ifNull : ["$tcWrkFlw.DADoneBy",""]}
                  }
         },
         {
             "$project" : {"tcWrkFlw" : 0,"daReviews" :0}
         }
    ]
 )        
 
 // End here Continuous Monitoring Status Report..


 // Script for Agency Performance Report
 db.getCollection('trainingcentre').aggregate([
	{
		"$project": {
			"reportName": "agencyPerformanceReport",

			"_id": 0,

			"tcId": {
				"$ifNull": ["$userName", ""]
			},

			"tcName": {
				"$ifNull": ["$trainingCentreName", ""]
			},

			"tcType": {
				"$ifNull": ["$type", ""]
			},

			"tpId": {
				"$ifNull": ["$trainingPartner.userName", ""]
			},

			"tpName": {
				"$ifNull": ["$trainingPartner.name", ""]
			},

			"tcAdress": {
				"$ifNull": ["$address.addressLine", ""]
			},

			"tcDist": {
				"$ifNull": ["$address.district.name", ""]
			},

            "tcStat": {
				"$ifNull": ["$address.state.name", ""]
			},

			"submissionDat": {
				"$ifNull": ["$submittedOn", ""]
			},

			"reInspctn": {
				"$ifNull": ["$canReInspect", ""]
			},


            "daReviews": {
				"$arrayElemAt": ["$daReviews", -1]
			},

            "withdrawal": "",

			"actStatus": {
				"$ifNull": ["$ActiveStatus", ""]
			},

			"SSCReviwDate": "",

			"totNumOfJobRol": {
				"$ifNull": ["$counter.jobroles", ""]
			},

			"totNmOfJobRolRecmdByQA": "",

            "totNmOfJobRolRecmdBySSC": "",

            "qaStatus": "",

            "sscStatus": "",

            "qcReviews": {
				"$arrayElemAt": ["$qcReviews", -1]
			},

			"jobRoles": {
				"$ifNull": ["$jobRoles", []]
			}
		}
	},
	{
		"$lookup": {
			"from": "tcworkflow",
			"let": {
				"username": "$userName"
			},
			"pipeline": [{
					"$match": {
						"$expr": {
							"$and": [{
									"$eq": ["$tcId", "$$username"]
								},
								{
									"$eq": ["$assignedNextUserRole", "Inspection Agency"]
								}
							]
						}
					}
				},
                {"$sort" : {"_id" : -1}},
                {"$limit" : 1},
				{
					"$project": {
						"insAgenNm": "$tcworkflow[0].spoc.firstName",
                        "insptnDate": "$tcworkflow[0].actionTakenOn",
						_id: 0
					}
				}
			],
			"as": "tcWrkFlw"
		}
	},
	{
		"$unwind": {
			"path": "$tcWrkFlw",
			"preserveNullAndEmptyArrays": true
		}
	},
	{
		"$addFields": {
			"desktopAssmntDate": {
				"$ifNull": ["$daReviews.date", ""]
			},
			"desktopAssmntStatus": {
				"$ifNull": ["$daReviews.review.status", ""]
			},
            "DAAgencyNm": {
				"$ifNull": ["$daReviews.userName", ""]
			},
            "insAgenNm": {
				"$ifNull": ["$tcWrkFlw.spoc.firstName", ""]
			},
             "insptnDate": {
				"$ifNull": ["$tcWrkFlw.actionTakenOn", ""]
			},
            "qcDate": {
				"$ifNull": ["$qcReviews.date", ""]
			}
		}
	},
	{
		"$addFields": {
            "jobRolNam": {
				"$reduce": {
					"input": "$jobRoles",
					"initialValue": "",
					"in": {
						"$cond": {
							"if": {
								"$eq": [{
									"$indexOfArray": ["$jobRoles", "$$this"]
								}, 0]
							},
							"then": {
								"$concat": ["$$value", "$$this.name"]
							},
							"else": {
								"$concat": ["$$value", ",", "$$this.name"]
							}
						}
					}
				}
			}
		}
	},
	{
		"$project": {
			"tcWrkFlw": 0,
			"daReviews": 0,
            "qcReviews": 0,
            "jobRoles" : 0
		}
	}
])


//Script for TP_TC_StatusReport
db.getCollection('trainingcentre').aggregate([
	// {
	{
		"$project": {
			"reportName": "TP_TC_StatusReport",
			"_id": 0,
			"tcId": {
				"$ifNull": ["$userName", ""]
			},
			"tcName": {
				"$ifNull": ["$trainingCentreName", ""]
			},
			"userName": {
				"$ifNull": ["$userName", ""]
			},
			"tcCreatnDate": {
				"$ifNull": ["$createdOn", ""]
			},
			"centrStatus": {
				"$ifNull": ["$tcstatus", ""]
			},
			"aplnNum": {
				"$ifNull": ["$ApplicationNumber", ""]
			},
			"scheme": {
				"$ifNull": ["$scheme", ""]
			},
			"tpId": {
				"$ifNull": ["$trainingPartner.userName", ""]
			},
			"tpName": {
				"$ifNull": ["$trainingPartner.name", ""]
			},
			"trainingCenterType": {
				"$ifNull": ["$trainingCenterType", ""]
			},
			"TC_spocMble": {
				"$ifNull": ["$spoc.mobileNumber", ""]
			},
			"TC_spocEmail": {
				"$ifNull": ["$spoc.email", ""]
			},
			"tcStat": {
				"$ifNull": ["$address.state.name", ""]
			},
			"tcDist": {
				"$ifNull": ["$address.district.name", ""]
			},
			"reInspctnStatus": {
				"$ifNull": ["$canReInspect", ""]
			},
			"taggedBy": {
				"$ifNull": ["$taggedBy", ""]
			},
			"TcCAAFAprvlStatus": {
				"$ifNull": ["$TcCAAFAprvlStatus", ""]
			},
			"daReviews": {
				"$arrayElemAt": ["$daReviews", -1]
			},
			"jobRoles": {
				"$ifNull": ["$jobRoles", []]
			},

		}
	},
	{
		"$lookup": {
			"from": "tcworkflow",
			"let": {
				"username": "$userName"
			},
			"pipeline": [{
					"$match": {
						"$expr": {
							"$and": [{
									"$eq": ["$tcId", "$$username"]
								},
								{
									"$eq": ["$assignedNextUserRole", "Centre Inspector"]
								}
							]
						}
					}
				},
				{
					"$project": {
						"proposeddate": "$otherInformation.centreinspection.proposeddate",
						_id: 0
					}
				}
			],
			"as": "tcWrkFlw"
		}
	},
	{
		"$unwind": {
			"path": "$tcWrkFlw",
			"preserveNullAndEmptyArrays": true
		}
	},
	{
		"$addFields": {
			"DA_Date": {
				"$ifNull": ["$daReviews.date", ""]
			},
			"DA_Status": {
				"$ifNull": ["$daReviews.review.status", ""]
			},
			"inspectionStatusDate": {
				"$ifNull": ["$tcWrkFlw.proposeddate", ""]
			},
			"Govt_TC": {
				"$cond": {
					if: {
						"$eq": ["$trainingCenterType", ""]
					},
					"then": '',
					else: {
						"$cond": {
							if: {
								"$eq": ["$trainingCenterType", "Government"]
							},
							"then": 'Yes',
							else: 'No'
						}
					}


				}
			}
		}
	},
	{
		"$addFields": {
			"qpCode": {
				"$reduce": {
					"input": "$jobRoles",
					"initialValue": "",
					"in": {
						"$cond": {
							"if": {
								"$eq": [{
									"$indexOfArray": ["$jobRoles", "$$this"]
								}, 0]
							},
							"then": {
								"$concat": ["$$value", "$$this.qp"]
							},
							"else": {
								"$concat": ["$$value", ",", "$$this.qp"]
							}
						}
					}
				}
			},
            "jbRoleStatus": {
				"$reduce": {
					"input": "$jobRoles",
					"initialValue": "",
					"in": {
						"$cond": {
							"if": {
								"$eq": [{
									"$indexOfArray": ["$jobRoles", "$$this"]
								}, 0]
							},
							"then": {
								"$concat": ["$$value", "$$this.status"]
							},
							"else": {
								"$concat": ["$$value", ",", "$$this.status"]
							}
						}
					}
				}
			},
            "jbRole": {
				"$reduce": {
					"input": "$jobRoles",
					"initialValue": "",
					"in": {
						"$cond": {
							"if": {
								"$eq": [{
									"$indexOfArray": ["$jobRoles", "$$this"]
								}, 0]
							},
							"then": {
								"$concat": ["$$value", "$$this.name"]
							},
							"else": {
								"$concat": ["$$value", ",", "$$this.name"]
							}
						}
					}
				}
			}
		}
	},
	{
		"$project": {
			"tcWrkFlw": 0,
			"daReviews": 0,
            "jobRoles" : 0
		}
	}
])