package db

import (
	"log"
	"nsdcexcelservice/config"

	mgo "gopkg.in/mgo.v2"
)

//GetSession :  connect to mongodb and gives sesson
func GetMgoSession() (*mgo.Database, *mgo.Session) {
	log.Println("connecting mongo...")
	uri := config.GetEnv("MONGODB_URL")
	mgoSession, err := mgo.Dial(uri)
	if err != nil {
		panic(err)
	}
	databaseName := config.GetEnv("DBNAME")
	db := mgoSession.DB(databaseName)
	return db, mgoSession
}
