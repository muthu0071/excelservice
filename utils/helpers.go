package utils

import (
	"fmt"
	"time"

	"gopkg.in/mgo.v2/bson"
)

// ConvertStringIst  ... Parsing the time Based on the Location @Params Input ISOstring {"2018-12-24T10:33:53.626Z"}
func ConvertStringIst(t string) time.Time {
	parsedTime, _ := time.Parse(time.RFC3339, t)
	countryLocation, _ := time.LoadLocation("Asia/Kolkata")
	return parsedTime.In(countryLocation)
}

func IfNullQueyry(val string) bson.M {

	return bson.M{
		"ifNull": []string{val, "NA"},
	}
}
func IfNullException(val string) bson.M {
	fmt.Println("00check")
	return bson.M{
		"ifNull": []string{val, "checkHell"},
	}
}
