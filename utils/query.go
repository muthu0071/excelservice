package utils

import (
	"nsdcexcelservice/constants"

	"gopkg.in/mgo.v2/bson"
)

func BuildProjectQueryForReport(reportName string) bson.M {

	projectQuery := bson.M{}
	switch true {
	case reportName == constants.RecomendataionsReport:
		projectQuery["$project"] = bson.M{
			"_id": 0,
			// "TP_ID":        "tpId",
			// "TP_NAME":      "tpSpocName",
			// "tpSpocEmail":  "tpSpocEmail",
			// "tpSpocMobile": "tpSpocMobile",
		}
	}
	return projectQuery
}
