package utils

import (
	"encoding/json"
	"fmt"
	"net/http"
)

// With500m : send response with 500 status and message
func With500m(w http.ResponseWriter, message string) {
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(500)
	fmt.Fprintf(w, `{"message": "%s"}`, message)
}

// With200 : Send Response with 200
// Content Type will be JSON
func With200(w http.ResponseWriter, data interface{}) {
	dataB, err := json.Marshal(data)
	if err != nil {
		w.WriteHeader(422)
		fmt.Fprintf(w, "Invalid Data")
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)
	w.Write(dataB)
}

// With200 : Send Response with 200
// Content Type will be JSON
func With200Aint(w http.ResponseWriter, data interface{}) {
	dataB, err := json.Marshal(data)
	if err != nil {
		w.WriteHeader(422)
		fmt.Fprintf(w, "Invalid Data")
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)
	w.Write(dataB)
}
