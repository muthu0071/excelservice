package handler

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
	"nsdcexcelservice/services"
	"nsdcexcelservice/utils"
)

func TestFileDownload(w http.ResponseWriter, r *http.Request) {

	_, serviceErr := services.DownloadReport()
	if serviceErr != nil {
		utils.With500m(w, serviceErr.Error())
		return
	}

	attachmentName := fmt.Sprintf("attachment; filename=excelReport.xlsx")
	w.Header().Set("Content-Disposition", attachmentName)
	w.Header().Set("Content-Type", "application/csv")
	streamPDFbytes, err := ioutil.ReadFile("./result.csv")
	if err != nil {
		utils.With500m(w, err.Error())
	}
	b := bytes.NewBuffer(streamPDFbytes)

	b.WriteTo(w)
}
