package handler

import (
	"encoding/json"
	"fmt"
	"net/http"
	"nsdcexcelservice/services"
	"nsdcexcelservice/utils"
	"time"

	"github.com/tealeg/xlsx"
)

type LiveHandler struct {
	Service services.ILiveService
}

func (l *LiveHandler) TestExcelReportJson(w http.ResponseWriter, r *http.Request) {

	if r.Method == "GET" {

		type ResponseDataStruct struct {
			Count int                 `json:"count"`
			Data  []map[string]string `json:"data"`
		}

		reponseData := ResponseDataStruct{}
		t1 := time.Now().UnixNano() / int64(time.Millisecond)

		check, count, err := l.Service.GetReportAsJson(r.URL.Query())
		t2 := time.Now().UnixNano() / int64(time.Millisecond)
		fmt.Println("-t2-t1", (t2-t1)/1000)
		if err != nil {
			utils.With500m(w, err.Error())
			return
		}
		reponseData.Data = check
		reponseData.Count = count
		utils.With200Aint(w, reponseData)
		return
	}
	var requestData map[string]interface{}
	err := json.NewDecoder(r.Body).Decode(&requestData)
	defer r.Body.Close()
	if err != nil {
		utils.With500m(w, err.Error())
		return
	}

	var file *xlsx.File
	var serviceErr error
	file, serviceErr = l.Service.GenerateExcelReport(requestData)
	if serviceErr != nil {
		utils.With500m(w, serviceErr.Error())
		return
	}
	attachmentName := fmt.Sprintf("attachment; filename=excelReport.xlsx")
	w.Header().Set("Content-Disposition", attachmentName)
	w.Header().Set("Content-Type", "application/vnd.ms-excel")
	errfILE := file.Write(w)
	if errfILE != nil {
		utils.With500m(w, errfILE.Error())
		return
	}
}
