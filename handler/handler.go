package handler

import (
	"encoding/json"
	"fmt"
	"net/http"
	"nsdcexcelservice/services"
	"nsdcexcelservice/utils"

	"github.com/tealeg/xlsx"
)

type Handler struct {
	Service services.IService
}

type IHandler interface {
	GenerateExcelReport(w http.ResponseWriter, r *http.Request)
}

func (h *Handler) GenerateExcelReport(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	var requestData map[string]interface{}
	type ResponseDataStruct struct {
		Count int                 `json:"count"`
		Data  []map[string]string `json:"data"`
	}
	reponseData := ResponseDataStruct{}

	if r.Method == http.MethodGet {
		jsonReportData, count, err1 := h.Service.GetReportAsJson(r.URL.Query())
		reponseData.Count = count
		reponseData.Data = jsonReportData
		if err1 != nil {
			utils.With500m(w, err1.Error())
			return
		}
		utils.With200Aint(w, reponseData)
		return
	}

	err := json.NewDecoder(r.Body).Decode(&requestData)
	if err != nil {
		utils.With500m(w, err.Error())
		return
	}

	var file *xlsx.File
	var serviceErr error
	file, serviceErr = h.Service.GenerateExcelReport(requestData)
	if serviceErr != nil {
		utils.With500m(w, serviceErr.Error())
		return
	}
	attachmentName := fmt.Sprintf("attachment; filename=excelReport.xlsx")
	w.Header().Set("Content-Disposition", attachmentName)
	w.Header().Set("Content-Type", "application/vnd.ms-excel")
	errfILE := file.Write(w)
	if errfILE != nil {
		utils.With500m(w, errfILE.Error())
		return
	}
}
