package services

import (
	"errors"
	"fmt"
	"nsdcexcelservice/constants"
	"nsdcexcelservice/dal"
	"strconv"

	"github.com/tealeg/xlsx"
)

type Service struct {
	Dal dal.IDal
}
type IService interface {
	GenerateExcelReport(queryData map[string]interface{}) (*xlsx.File, error)
	GetReportAsJson(queryData map[string][]string) ([]map[string]string, int, error)
}

func (s *Service) GenerateExcelReport(queryData map[string]interface{}) (*xlsx.File, error) {

	var file *xlsx.File
	var sheet *xlsx.Sheet
	var row *xlsx.Row
	var cell *xlsx.Cell
	file = xlsx.NewFile()
	sheet, err := file.AddSheet("Sheet1")
	if err != nil {
		return nil, err
	}
	columnData, errColumData := GenerateColumnName(queryData["reportName"].(string))
	if errColumData != nil {
		return nil, errColumData
	}
	row = sheet.AddRow()
	for i := 0; i < len(columnData); i++ {
		cell = row.AddCell()
		cell.Value = columnData[i]
	}

	file, err1 := GenerateRowsForReport(queryData, file, sheet, row, cell)

	fmt.Println("--after rows Report")
	if err1 != nil {
		return nil, err1
	}
	return file, nil
}
func (s *Service) GetReportAsJson(queryData map[string][]string) ([]map[string]string, int, error) {
	findQuery := map[string]interface{}{}
	skipValue, errItemsPage := strconv.Atoi(queryData["pageNo"][0])
	limitValue, errPageNo := strconv.Atoi(queryData["itemsperpage"][0])
	if errPageNo != nil || errItemsPage != nil {
		return nil, 0, errors.New("Please provide A valit items perpage and  pageNumber")
	}
	for val, k := range queryData {
		if len(k) != 0 {
			findQuery[val] = k[0]
		}
	}
	delete(findQuery, "itemsperpage")
	delete(findQuery, "pageNo")
	skipValue = skipValue*limitValue - 1
	return s.Dal.FindReportAsJson(findQuery, skipValue, limitValue-1)
}

func GenerateColumnName(processType string) ([]string, error) {
	//1st for TPTCStatusReport
	if processType == constants.TPTCStatusReport {
		TPTCStatusReport := []string{
			"S.NO", "TP ID", "TP NAME", "TC ID", "TC CREATION DATE", "APPLICATION NO",
			"TC NAME", "TC SPOC MOBILE", "TC SPOC EMAIL ID", "GOVT. TC", "SCHEME", "RECOMMENDED BY",
			"STATE", "DISTRICT", "DA STATUS", "DA DATE", "CENTER STATUS", "INSPECTION STATUS DATE",
			"JOB ROLE STATUS", "RE-INSPECTION STATUS",
			"RE-INSPECTION DATE", "QP CODE", "JOB ROLE", "Tagged by", "TC CAAF Approval Status"}
		return TPTCStatusReport, nil

		//2nd for RecomendationReport
	} else if processType == constants.RecomendataionsReport {
		RecomendationReport := []string{
			"S.NO", "TP_ID", "TP_Name", "TP_SPOC_Name", "TP_SPOC_E_Mail_ID",
			"TP_SPOC_Mobile_No", "TC_ID", "TC_NAME",
			"TC_SPOC_Name", "SPOC_Mobile", "SPOC_E_Mail_ID",
			"APPLICATION_NO", "SCHEME", "SUBMISSION_DATE",
			"DA_Report_Link", "Inspection_Report_Link",
			"TC_Type", "STATEID", "STATE_NAME",
			"FULL_ADDRESS", "Pincode", "DA_STATUS",
			"DISTRICT_NAME", "CONSTITUENCY_NAME",
			"DA_DATE", "ACTUAL_INSPECTION_DATE",
			"TC_REMARKS", "ALLOCATED_INSPECTION_DATE",
			"INSPECTION_AGENCY", "RE_INSPECTION_APPLY_STATUS",
			"RE_INSPECTION_APPLY_DATE", "ACTUAL_RE_INSPECTION_DATE",
			"INSPECTION_REPORT_SUBMISSION_DATE", "CENTER_QUALIFICATION_STATUS",
			"IA_RECOMMENDATION_STATUS", "IA_RECOMMENDATION_DATE",
			"IA_REMARKS", "QP_CODE", "JOB_ROLE",
			"SSC_NAME", "RATING", "SCORE", "CARPET_AREA",
			"SSC_STATUS_DATE", "OLD_SSC_STATUS_DATE",
			"SSC_ACCREDITATION_STATUS", "OLD_SSC_ACCREDITATION_STATUS",
			"SSC_REMARKS", "AFFILIATION_STATUS",
			"AFFILIATION_STATUS_DATE", "OLD_AFFILIATION_STATUS",
			"OLD_AFFILIATION_STATUS_DATE", "CONTINUOUS_MONITORING_FEE",
			"CERTIFICATION_STATUS"}
		return RecomendationReport, nil

		//3rd for AgencyPerformaceReport
	} else if processType == constants.AgencyPerformanceReport {
		AgencyPerformaceReport := []string{
			"Sr.No.", "TP Name", "TP Id", "TC Name",
			"TC Id", "TC Type", "State", "District",
			"TC Address", "Submission Date", "Re inspection (Yes/No)", "Desktop Assessment Date", "Desktop Assessment Status", "Withdrawal", "Active Status",
			"DA Agency Name", "Inspection Agency Name", "Inspection Date", "QC Date",
			"SSC Review Date", "Total No of Job Roles", "Total No of Job roles recommended by Quality Agency", "Total No of Job roles recommended by SSC",
			"Job Role Name", "Quality Agency Status", "SSC Status"}
		return AgencyPerformaceReport, nil

		//4th for RowdataForReport
	} else if processType == "RowdataForReport" {
		RowdataForReport := []string{
			"Result Name", "Report Type",
			"Total TP Registered ", "Total TP Payment Made", "TC Registered",
			"TC Payments for Application Fee",
			"TC Payments for Continious Monitoring Fee", "TC Payments for Re-Inspection Application Fee",
			"TC Payments for Affiliation Fee", "TP DR", "TP DNR",
			"TC DR", "TC DNR", "Number of Inspections Assigned to IMACS",
			"Number of Inspections Done by IMACS", "Number of Inspections Assigned to QUACA",
			"Number of Inspections Done By QUACA", "TC Accredited By SSC",
			"TC Conditional Accredited By SSC",
			"TC Not Accredited By SSC", "Number of TCs Affiliated", "Number of TC to TP Conversion"}
		return RowdataForReport, nil

		//5th for TCRefundDetails
	} else if processType == constants.TcRefundReport {
		TCRefundDetails := []string{
			"Sr No.", "Tp ID", "Tp Name", "TC Type", "TC ID", "TC NAME",
			"Center status", "merchantId", "transaction ID", "type Of Fees", "fees Amount", "transaction Result"}
		return TCRefundDetails, nil

		//6th for TargetAllocationReport
	} else if processType == constants.TargetAllocationReport {
		TargetAllocationReport := []string{
			"Sr.No.", "TC Application No.", "Affiliation Status", "Affiliation Date", "TC Username", "TP Application No.", "TP Username", "Star Grading as per Inspection Report", "Center Carpet Area as per inspection Report",
			"Type of Inspection (1st/Reinspection)",
			"Type of Training Centre",
			"State", "District", "Parliamentary Constituency", "TP Name", "TC Name",
			"Sector Skill Council", "Job Role Name",
			"QP Code", "Center Qualify Status",
			"JobRole IA Status", "MaximumAllowedCapacity", "Class Name", "Classroom Area", "No. of Hybrid Classroom", "Unique No. of Classrooms",
			"Labroom Area", "Lab Name", "Unique No. of Labs", "No. of Dedicated Trainers",
			"TC Spoc Name", "TC Spoc Mobile", "TC Spoc Email", "Address", "Pincode",
			"TP Spoc Name", "TP Spoc Email",
			"TP Spoc Mobile"}
		return TargetAllocationReport, nil

		//7th for continuousMonitoring
	} else if processType == constants.ContinuousReport {
		continuousMonitoring := []string{
			"S.NO", "TC ID", "TP ID", "TC NAME", "DA Agency",
			"DA Done by", "DA Assigned on", "DA Done on", "Remarks"}
		return continuousMonitoring, nil

		//8th for paymentDetails
	} else if processType == constants.RecomendataionsReport {
		PaymentDetails := []string{
			"S. No.", "Download Invoice", "Status",
			"Payment By", "Order/Payment ID", "Transaction ID", "Merchant ID",
			"Invoice No.", "TP Application No.",
			"TC Application No.", "Name", "User Name",
			"TP ID", "Type of Training center",
			"Fee Description", "SSC Name", "QP Code",
			"Application Fees", "Job Role Fees", "Conv Fees", "Total Amount", "Transaction Date", "Transaction Result", "Address", "State", "District",
			"Payment Mode", "Refund Date", "Reference No.", "Refund Status", "Refund Mode", "Refund By",
			"Refund Amount", "Remarks"}
		return PaymentDetails, nil
	} else if processType == constants.TpReport {
		tpReports := []string{
			"S. No.", "TP ID", "TPNAME",
			"SpocMobileNO", "SpocEmailID", "Scheme", "State",
			"Allow TC", "CreatedTC",
			"Pending TC", "Recomended By", "DAStatus"}
		return tpReports, nil
	} else if processType == constants.TcStatusReports {
		tcStatus := []string{
			"S. No.", "TP ID", "TPNAME",
			"TCID", "TC CREATION", "TCNAME", "TC SPOC MOBILE",
			"TC SPOC EMAIL", "SCHEME",
			"RECOMENDED By", "STATE",
			"DISTRICT", "CENTER STATUS", "TC CAFF APPPROVAL status"}
		return tcStatus, nil
	} else if processType == constants.PaymentDetailsReport {
		fmt.Println("---paymentDeatilscheckforColumn")
		paymentDetails := []string{
			"S.No", "Download Invoice", "status", "PaymentBy", "Order paymentId",
			"TransactionId", "MerchantId", "Invoice No.", "TP Application No.", "Tc Application No.", "Name", "User Name", "Tp Id",
			"Type of Training center", "Fee Description", "SSC Name", "QpCode", "Application Fees", "Job Role Fees", "conv Fees", "TOtal Amount",
			"Transaction on Date", "Transaction on Result", "Address", "State", "District", "Payment Mode",
			"Refund Date", "Reference No.", "Refund Status", "Refund Mode", "Refund By",
			"Refund Amount", "Remarks",
		}
		return paymentDetails, nil
	} else {
		return nil, errors.New("Please provide a valid Report Type")
	}
}

func GenerateRowsForReport(inputData map[string]interface{}, file *xlsx.File, sheet *xlsx.Sheet, row *xlsx.Row, cell *xlsx.Cell) (*xlsx.File, error) {
	dalInst := &dal.Dal{}
	reportName := inputData["reportName"].(string)
	finalData, er := dalInst.GetReportData(inputData)
	if er != nil {
		return nil, er
	}
	var err error
	switch true {
	case reportName == constants.RecomendataionsReport:
		{
			fmt.Println("--check for generateReport")
			file, err = GenerateRowsForRecomendationReport(file, finalData, sheet, row, cell)
			if err != nil {
				return nil, err
			}
		}
	case reportName == constants.AgencyPerformanceReport:
		{
			file, err = GenerateRowsForAgencyPerformanceReport(file, finalData, sheet, row, cell)
			if err != nil {
				return nil, err
			}
		}
	case reportName == constants.ContinuousReport:
		{
			file, err = GenerateRowsForContinuousReport(file, finalData, sheet, row, cell)
			if err != nil {
				return nil, err
			}

		}
		// incompleted data mismatch
	case reportName == constants.TargetAllocationReport:
		{
			file, err = GenerateRowsForTargetAllocationReport(file, finalData, sheet, row, cell)
			if err != nil {
				return nil, err
			}

		}
	case reportName == constants.TpReport:
		{

			file, err = GenerateRowsForTpReport(file, finalData, sheet, row, cell)
			if err != nil {
				return nil, err
			}
		}
	case reportName == constants.TcStatusReports:
		{
			file, err = GenerateRowsFortcStatusReports(file, finalData, sheet, row, cell)
			if err != nil {
				return nil, err
			}
		}
	case reportName == constants.TcRefundReport:
		{
			file, err = GenerateRowsForPaymentRefundReports(file, finalData, sheet, row, cell)
			if err != nil {
				return nil, err
			}
		}
	case reportName == constants.PaymentDetailsReport:
		{
			file, err = GenerateRowsForPaymentDeatilsReportsfile(file, finalData, sheet, row, cell)
			if err != nil {
				return nil, err
			}
		}

	case reportName == constants.TPTCStatusReport:
		{

			file, err = GenerateRowsForTpTcStatusReports(file, finalData, sheet, row, cell)
			if err != nil {
				return nil, err
			}
		}

	default:
		return nil, errors.New("Please provide a  valid reportNameFor gen Rows")
	}

	return file, nil
}
