package services

import (
	"context"
	"encoding/csv"
	"fmt"
	"log"
	"nsdcexcelservice/constants"
	"nsdcexcelservice/dal"
	"os"
	"strconv"
	"time"

	"gopkg.in/mgo.v2/bson"
)

// retrun a file
func DownloadReport() (*os.File, error) {

	_, err := dal.GetCountOfagencyPerCollection()
	if err != nil {
		return nil, err
	}

	t1 := time.Now().UnixNano() / int64(time.Millisecond)
	// _, _, _ = val[constants.AgencyPerformanceReport](nil, 1, 5000)
	fmt.Print("--insidegoroute")

	t2 := time.Now().UnixNano() / int64(time.Millisecond)
	fmt.Println("--timeinMillisec", t2-t1)
	file, err := os.Create("./result.csv")
	defer file.Close()
	fmt.Println("--checkit out")
	writer := csv.NewWriter(file)
	defer writer.Flush()

	t3 := time.Now().UnixNano() / int64(time.Millisecond)

	fmt.Println("-------t2", t3-t2)
	value, err := dal.ConnectMongodb()

	heck1 := []map[string]string{}
	t31 := time.Now().UnixNano() / int64(time.Millisecond)

	ctx := context.Background()
	for value.Next(ctx) {
		var result bson.M
		err := value.Decode(&result)
		appendValue := map[string]string{}
		for key1, val1 := range result {
			appendValue[key1] = fmt.Sprintf("%v", val1)
		}
		if err != nil {
			log.Fatal(err)
		}
		heck1 = append(heck1, appendValue)
	}

	value.Close(ctx)
	t4 := time.Now().UnixNano() / int64(time.Millisecond)
	fmt.Println("---t31", t31-t4)
	heck1 = append(heck1, heck1[0])
	valueForCsv, _ := GenerateRowsForAgencyPerformanceReportV2(heck1)
	err = writer.WriteAll(valueForCsv)
	t5 := time.Now().UnixNano() / int64(time.Millisecond)
	fmt.Println("--t3", t5-t4)
	return file, nil

}

func GenerateRowsForAgencyPerformanceReportV2(finaldataForReport []map[string]string) ([][]string, error) {

	columnData, _ := GenerateColumnName(constants.AgencyPerformanceReport)
	finalValue := [][]string{}
	finalValue = append(finalValue, columnData)
	if len(finaldataForReport) != 0 {
		for Sno, v := range finaldataForReport {
			outValue := []string{
				strconv.Itoa(Sno + 1), v["tpName"], v["tpId"], v["tcName"], v["tcId"],
				v["tcType"], v["tcStat"], v["tcDist"], v["tcAdress"], v["submissionDat"],
				v["reInspctn"], v["desktopAssmntDate"], v["desktopAssmntStatus"], v["withdrawal"],
				v["actStatus"], v["DAAgencyNm"], v["insAgenNm"], v["insptnDate"], v["qcDate"],
				v["SSCReviwDate"], v["totNumOfJobRol"], v["totNmOfJobRolRecmdByQA"], v["totNmOfJobRolRecmdBySSC"],
				v["jobRolNam"], v["qaStatus"], v["sscStatus"],
			}
			finalValue = append(finalValue, outValue)
		}
	}
	return finalValue, nil
}
