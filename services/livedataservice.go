package services

import (
	"errors"
	"fmt"
	"nsdcexcelservice/constants"
	"nsdcexcelservice/dal"
	"strconv"

	"github.com/tealeg/xlsx"
)

type LiveService struct {
	Dal dal.ILiveDal
}
type ILiveService interface {
	GetReportAsJson(queryData map[string][]string) ([]map[string]string, int, error)
	GenerateExcelReport(queryData map[string]interface{}) (*xlsx.File, error)
}

func (l *LiveService) GetReportAsJson(queryData map[string][]string) ([]map[string]string, int, error) {
	findQuery := map[string]interface{}{}
	limitValue, errPageNo := strconv.Atoi(queryData["itemsperpage"][0])
	skipValue, errItemsPage := strconv.Atoi(queryData["pageNo"][0])
	if errPageNo != nil || errItemsPage != nil {
		return nil, 0, errors.New("Please provide A valit items perpage and  pageNumber")
	}
	for val, k := range queryData {
		if len(k) != 0 {
			findQuery[val] = k[0]
		}
	}
	delete(findQuery, "itemsperpage")
	delete(findQuery, "pageNo")
	skipValue = skipValue * limitValue
	reportName := findQuery["reportName"].(string)
	finalJsonData := l.Dal.GetReportJsonData()

	// fmt.Println("---reportName JsonData", reportName)
	// finaldalData := dalInst.GetReportJsonData()
	daljsonFunc, ok := finalJsonData[reportName]

	if ok {
		return daljsonFunc(findQuery, skipValue, limitValue)

	} else {
		return nil, 0, errors.New("Please Provide the valid Report name")
	}
}

func (l *LiveService) GenerateExcelReport(queryData map[string]interface{}) (*xlsx.File, error) {

	var file *xlsx.File
	var sheet *xlsx.Sheet
	var row *xlsx.Row
	var cell *xlsx.Cell
	file = xlsx.NewFile()
	sheet, err := file.AddSheet("Sheet1")
	if err != nil {
		return nil, err
	}
	columnData, errColumData := GenerateColumnName(queryData["reportName"].(string))
	if errColumData != nil {
		return nil, errColumData
	}
	row = sheet.AddRow()
	for i := 0; i < len(columnData); i++ {
		cell = row.AddCell()
		cell.Value = columnData[i]
	}

	var err1 error

	file, err1 = GenerateRowsForReportLive(queryData, file, sheet, row, cell)

	if err1 != nil {
		return nil, err1
	}
	return file, nil
}

func GenerateRowsForReportLive(inputData map[string]interface{}, file *xlsx.File, sheet *xlsx.Sheet, row *xlsx.Row, cell *xlsx.Cell) (*xlsx.File, error) {
	dalInst := &dal.LiveDal{}
	reportName := inputData["reportName"].(string)

	finaldalData := dalInst.GetReportJsonData()
	finalData := []map[string]string{}
	var err error
	finalData, _, err = finaldalData[reportName](inputData, 0, 0)
	fmt.Println("--err", err)
	if err != nil {
		return nil, err
	}
	switch true {
	case reportName == constants.RecomendataionsReport:
		{
			file, err = GenerateRowsForRecomendationReport(file, finalData, sheet, row, cell)
			if err != nil {
				return nil, err
			}
		}
	case reportName == constants.AgencyPerformanceReport:
		{
			file, err = GenerateRowsForAgencyPerformanceReport(file, finalData, sheet, row, cell)
			if err != nil {
				return nil, err
			}
		}
	case reportName == constants.ContinuousReport:
		{
			file, err = GenerateRowsForContinuousReport(file, finalData, sheet, row, cell)
			if err != nil {
				return nil, err
			}

		}
		// incompleted data mismatch
	case reportName == constants.TargetAllocationReport:
		{
			file, err = GenerateRowsForTargetAllocationReport(file, finalData, sheet, row, cell)
			if err != nil {
				return nil, err
			}

		}
	case reportName == constants.TpReport:
		{

			file, err = GenerateRowsForTpReport(file, finalData, sheet, row, cell)
			if err != nil {
				return nil, err
			}
		}
	case reportName == constants.TcStatusReports:
		{
			file, err = GenerateRowsFortcStatusReports(file, finalData, sheet, row, cell)
			if err != nil {
				return nil, err
			}
		}
	case reportName == constants.TcRefundReport:
		{
			file, err = GenerateRowsForPaymentRefundReports(file, finalData, sheet, row, cell)
			if err != nil {
				return nil, err
			}
		}
	case reportName == constants.PaymentDetailsReport:
		{

			file, err = GenerateRowsForPaymentDeatilsReportsfile(file, finalData, sheet, row, cell)
			if err != nil {
				return nil, err
			}
		}

	case reportName == constants.TPTCStatusReport:
		{

			file, err = GenerateRowsForTpTcStatusReports(file, finalData, sheet, row, cell)
			if err != nil {
				return nil, err
			}
		}

	default:
		return nil, errors.New("Please provide a  valid reportNameFor gen Rows")
	}

	return file, nil
}
