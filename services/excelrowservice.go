package services

import (
	"strconv"

	"github.com/tealeg/xlsx"
)

func GenerateRowsForRecomendationReport(file *xlsx.File, finaldataForReport []map[string]string, sheet *xlsx.Sheet, row *xlsx.Row, cell *xlsx.Cell) (*xlsx.File, error) {

	if len(finaldataForReport) != 0 {
		for Sno, v := range finaldataForReport {
			row = sheet.AddRow()
			cell = row.AddCell()
			cell.Value = strconv.Itoa(Sno + 1)
			cell = row.AddCell()
			cell.Value = v["tpId"]
			cell = row.AddCell()
			cell.Value = v["tpName"]
			cell = row.AddCell()
			cell.Value = v["tpSpocName"]
			cell = row.AddCell()
			cell.Value = v["tpSpocEmail"]
			cell = row.AddCell()
			cell.Value = v["tpSpocMobile"]
			cell = row.AddCell()
			cell.Value = v["tcId"]
			cell = row.AddCell()
			cell.Value = v["tcName"]
			cell = row.AddCell()
			cell.Value = v["tcSpocName"]
			cell = row.AddCell()
			cell.Value = v["tcSpocMobile"]
			cell = row.AddCell()
			cell.Value = v["tcSpocEMail"]
			cell = row.AddCell()
			cell.Value = v["applicatinNo"]
			cell = row.AddCell()
			cell.Value = v["tcSubmissionDt"]
			cell = row.AddCell()
			cell.Value = v["daReportLink"]
			cell = row.AddCell()
			cell.Value = v["inspectionReportLink"]
			cell = row.AddCell()
			cell.Value = v["tcType"]
			cell = row.AddCell()
			cell.Value = v["tcStateId"]
			cell = row.AddCell()
			cell.Value = v["tcStateName"]
			cell = row.AddCell()
			cell.Value = v["tcDistName"]

			cell = row.AddCell()
			cell.Value = v["tcConstituencyName"]

			cell = row.AddCell()
			cell.Value = v["tcFullAddrss"]

			cell = row.AddCell()
			cell.Value = v["tcPincode"]

			cell = row.AddCell()
			cell.Value = v["daStatus"]

			cell = row.AddCell()
			cell.Value = v["daDate"]

			cell = row.AddCell()
			cell.Value = v["actualInspectionDate"]

			cell = row.AddCell()
			cell.Value = v["actualInspectionDate"]
		}
	}
	return file, nil
}

func GenerateRowsForAgencyPerformanceReport(file *xlsx.File, finaldataForReport []map[string]string, sheet *xlsx.Sheet, row *xlsx.Row, cell *xlsx.Cell) (*xlsx.File, error) {

	if len(finaldataForReport) != 0 {
		for Sno, v := range finaldataForReport {
			row = sheet.AddRow()
			cell = row.AddCell()
			cell.Value = strconv.Itoa(Sno + 1)
			cell = row.AddCell()
			cell.Value = v["tpName"]
			cell = row.AddCell()
			cell.Value = v["tpId"]
			cell = row.AddCell()
			cell.Value = v["tcName"]
			cell = row.AddCell()
			cell.Value = v["tcId"]
			cell = row.AddCell()
			cell.Value = v["tcType"]
			cell = row.AddCell()
			cell.Value = v["tcStat"]
			cell = row.AddCell()
			cell.Value = v["tcDist"]
			cell = row.AddCell()
			cell.Value = v["tcAdress"]
			cell = row.AddCell()
			cell.Value = v["submissionDat"]
			cell = row.AddCell()
			cell.Value = v["reInspctn"]
			cell = row.AddCell()
			cell.Value = v["desktopAssmntDate"]
			cell = row.AddCell()
			cell.Value = v["desktopAssmntStatus"]
			cell = row.AddCell()
			cell.Value = v["withdrawal"]
			cell = row.AddCell()
			cell.Value = v["actStatus"]
			cell = row.AddCell()
			cell.Value = v["DAAgencyNm"]
			cell = row.AddCell()
			cell.Value = v["insAgenNm"]
			cell = row.AddCell()
			cell.Value = v["insptnDate"]
			cell = row.AddCell()
			cell.Value = v["qcDate"]

			cell = row.AddCell()
			cell.Value = v["SSCReviwDate"]

			cell = row.AddCell()
			cell.Value = v["totNumOfJobRol"]

			cell = row.AddCell()
			cell.Value = v["totNmOfJobRolRecmdByQA"]

			cell = row.AddCell()
			cell.Value = v["totNmOfJobRolRecmdBySSC"]

			cell = row.AddCell()
			cell.Value = v["jobRolNam"]

			cell = row.AddCell()
			cell.Value = v["qaStatus"]

			cell = row.AddCell()
			cell.Value = v["sscStatus"]
		}
	}
	return file, nil
}

func GenerateRowsForContinuousReport(file *xlsx.File, finaldataForReport []map[string]string, sheet *xlsx.Sheet, row *xlsx.Row, cell *xlsx.Cell) (*xlsx.File, error) {

	if len(finaldataForReport) != 0 {
		for Sno, v := range finaldataForReport {
			row = sheet.AddRow()
			cell = row.AddCell()
			cell.Value = strconv.Itoa(Sno + 1)
			cell = row.AddCell()
			cell.Value = v["tcId"]
			cell = row.AddCell()
			cell.Value = v["tpId"]
			cell = row.AddCell()
			cell.Value = v["tcName"]
			cell = row.AddCell()
			cell.Value = v["DAAgency"]
			cell = row.AddCell()
			cell.Value = v["DADoneBy"]
			cell = row.AddCell()
			cell.Value = v["DAAsingndOn"]
			cell = row.AddCell()
			cell.Value = v["DADoneOn"]
			cell = row.AddCell()
			cell.Value = v["remark"]
			cell = row.AddCell()
		}
	}
	return file, nil
}

func GenerateRowsForTargetAllocationReport(file *xlsx.File, finaldataForReport []map[string]string, sheet *xlsx.Sheet, row *xlsx.Row, cell *xlsx.Cell) (*xlsx.File, error) {
	if len(finaldataForReport) != 0 {
		for Sno, v := range finaldataForReport {
			row = sheet.AddRow()
			cell = row.AddCell()
			cell.Value = strconv.Itoa(Sno + 1)
			cell = row.AddCell()
			cell.Value = v["tcapplicationNumber"]
			cell = row.AddCell()
			cell.Value = v["affiliationStatus"]
			cell = row.AddCell()
			cell.Value = v["affiliationDate"]
			cell = row.AddCell()
			cell.Value = v["tcUserName"]
			cell = row.AddCell()
			cell.Value = v["tpAppNumber"]
			cell = row.AddCell()
			cell.Value = v["tpName"]
			cell = row.AddCell()
			cell.Value = v["DADoneOn"]
			cell = row.AddCell()
			cell.Value = v["remark"]
			cell = row.AddCell()
		}
	}
	return file, nil
}

func GenerateRowsForTpReport(file *xlsx.File, finaldataForReport []map[string]string, sheet *xlsx.Sheet, row *xlsx.Row, cell *xlsx.Cell) (*xlsx.File, error) {
	dataLen := len(finaldataForReport)

	if dataLen != 0 {

		for i := 0; i < dataLen; i++ {
			// fmt.Println("--i", i)
			row = sheet.AddRow()
			cell = row.AddCell()
			cell.Value = strconv.Itoa(i + 1)
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["tpId"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["tpName"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["tpSpocMob"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["tpSpocEmail"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["scheme"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["state"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["allowTc"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["createdTc"]
			cell = row.AddCell()
		}
	}
	return file, nil

}

func GenerateRowsFortcStatusReports(file *xlsx.File, finaldataForReport []map[string]string, sheet *xlsx.Sheet, row *xlsx.Row, cell *xlsx.Cell) (*xlsx.File, error) {

	dataLen := len(finaldataForReport)

	if dataLen != 0 {

		for i := 0; i < dataLen; i++ {
			// fmt.Println("--i", i)
			row = sheet.AddRow()
			cell = row.AddCell()
			cell.Value = strconv.Itoa(i + 1)
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["trainingPartneruserName"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["trainingPartnername"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["trainingcentreuserName"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["createdOn"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["trainingCentreName"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["spocmobileNumber"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["spocemail"]
			cell = row.AddCell()

			cell.Value = finaldataForReport[i]["recomendedBy"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["addressstatename"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["addressdistrictname"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["status"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["tcstatus"]
			cell = row.AddCell()
		}
	}
	return file, nil

}
func GenerateRowsForPaymentRefundReports(file *xlsx.File, finaldataForReport []map[string]string, sheet *xlsx.Sheet, row *xlsx.Row, cell *xlsx.Cell) (*xlsx.File, error) {
	dataLen := len(finaldataForReport)

	if dataLen != 0 {
		for i := 0; i < dataLen; i++ {
			row = sheet.AddRow()
			cell = row.AddCell()
			cell.Value = strconv.Itoa(i + 1)
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["tpID"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["tpName"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["tcType"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["tcID"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["tcName"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["tcStatus"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["merchantId"]
			cell = row.AddCell()

			cell.Value = finaldataForReport[i]["trxnID"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["feeType"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["refndAmnt"]
			cell = row.AddCell()

			cell.Value = finaldataForReport[i]["trxnResult"]
			cell = row.AddCell()
			// trxnDate
			cell.Value = finaldataForReport[i]["trxnDate"]
			cell = row.AddCell()

		}
	}
	return file, nil
}

func GenerateRowsForPaymentDeatilsReportsfile(file *xlsx.File, finaldataForReport []map[string]string, sheet *xlsx.Sheet, row *xlsx.Row, cell *xlsx.Cell) (*xlsx.File, error) {
	dataLen := len(finaldataForReport)
	if dataLen != 0 {
		for i := 0; i < dataLen; i++ {
			row = sheet.AddRow()
			cell = row.AddCell()
			cell.Value = strconv.Itoa(i + 1)
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["receiptPath"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["status"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["paymentBy"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["orderId"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["tranctnID"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["merchantId"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["invoiceNo"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["tpAplnNo"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["tcAplnNo"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["name"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["userName"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["tpId"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["typeOfTc"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["feeDescription"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["sscName"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["qpCode"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["applicationFees"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["jobRoleFees"]
			cell = row.AddCell()

			cell.Value = finaldataForReport[i]["convFees"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["totalAmount"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["transactiondateFormat"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["transactionResult"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["address"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["state"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["district"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["paymentMode"]
			cell = row.AddCell()
			// refundDate
			cell.Value = finaldataForReport[i]["refundDate"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["referenceNo"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["refundStatus"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["refundMode"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["refundBy"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["convertedRefuncAmount"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["remarks"]
			cell = row.AddCell()

		}
	}
	return file, nil
}

func GenerateRowsForTpTcStatusReports(file *xlsx.File, finaldataForReport []map[string]string, sheet *xlsx.Sheet, row *xlsx.Row, cell *xlsx.Cell) (*xlsx.File, error) {
	dataLen := len(finaldataForReport)
	if dataLen != 0 {
		for i := 0; i < dataLen; i++ {
			row = sheet.AddRow()
			cell = row.AddCell()
			cell.Value = strconv.Itoa(i + 1)
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["tpId"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["tpName"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["tcId"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["tcCreatnDate"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["aplnNum"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["tcName"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["TC_spocMble"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["TC_spocEmail"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["Govt_TC"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["scheme"]
			cell = row.AddCell()

			cell.Value = finaldataForReport[i]["recomendedBy"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["state"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["district"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["DA_Status"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["DA_Date"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["centrStatus"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["inspectionStatusDate"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["jbRoleStatus"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["reInspctnStatus"]
			cell = row.AddCell()

			cell.Value = finaldataForReport[i]["reInspctnDate"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["qpCode"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["taggedBy"]
			cell = row.AddCell()
			cell.Value = finaldataForReport[i]["TcCAAFAprvlStatus"]
			cell = row.AddCell()
		}
	}
	return file, nil
}

func GenerateRowsForAgencyPerformanceReportTestGo(file *xlsx.File, finaldataForReport []map[string]string, sheet *xlsx.Sheet, row *xlsx.Row, cell *xlsx.Cell) (*xlsx.File, error) {

	if len(finaldataForReport) != 0 {
		for Sno, v := range finaldataForReport {
			row = sheet.AddRow()
			cell = row.AddCell()
			cell.Value = strconv.Itoa(Sno + 1)
			cell = row.AddCell()
			cell.Value = v["tpName"]
			cell = row.AddCell()
			cell.Value = v["tpId"]
			cell = row.AddCell()
			cell.Value = v["tcName"]
			cell = row.AddCell()
			cell.Value = v["tcId"]
			cell = row.AddCell()
			cell.Value = v["tcType"]
			cell = row.AddCell()
			cell.Value = v["tcStat"]
			cell = row.AddCell()
			cell.Value = v["tcDist"]
			cell = row.AddCell()
			cell.Value = v["tcAdress"]
			cell = row.AddCell()
			cell.Value = v["submissionDat"]
			cell = row.AddCell()
			cell.Value = v["reInspctn"]
			cell = row.AddCell()
			cell.Value = v["desktopAssmntDate"]
			cell = row.AddCell()
			cell.Value = v["desktopAssmntStatus"]
			cell = row.AddCell()
			cell.Value = v["withdrawal"]
			cell = row.AddCell()
			cell.Value = v["actStatus"]
			cell = row.AddCell()
			cell.Value = v["DAAgencyNm"]
			cell = row.AddCell()
			cell.Value = v["insAgenNm"]
			cell = row.AddCell()
			cell.Value = v["insptnDate"]
			cell = row.AddCell()
			cell.Value = v["qcDate"]

			cell = row.AddCell()
			cell.Value = v["SSCReviwDate"]

			cell = row.AddCell()
			cell.Value = v["totNumOfJobRol"]

			cell = row.AddCell()
			cell.Value = v["totNmOfJobRolRecmdByQA"]

			cell = row.AddCell()
			cell.Value = v["totNmOfJobRolRecmdBySSC"]

			cell = row.AddCell()
			cell.Value = v["jobRolNam"]

			cell = row.AddCell()
			cell.Value = v["qaStatus"]

			cell = row.AddCell()
			cell.Value = v["sscStatus"]
		}
	}
	return file, nil
}
