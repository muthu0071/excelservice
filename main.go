package main

import (
	"log"
	"net/http"
	"nsdcexcelservice/config"
	"nsdcexcelservice/dal"
	"nsdcexcelservice/handler"
	"nsdcexcelservice/services"
	"strings"

	"github.com/gorilla/mux"
	"github.com/rs/cors"
)

func main() {
	serverPort := config.GetEnv("PORT")
	log.Println("Your excelservice server Running on" + serverPort)
	// t1 := time.Now().UnixNano() / int64(time.Millisecond)

	// t2 := time.Now().UnixNano() / int64(time.Millisecond)

	// fmt.Println("--err", err, "--tiemcompe", t2-t1)
	handler := getCorsEnabled(CheckRoutes())
	http.ListenAndServe(serverPort, handler)
}

func getCorsEnabled(r *mux.Router) http.Handler {
	AllowedOrigins := config.GetEnv("AllowedOrigins")

	arrayAll := strings.Split(AllowedOrigins, ",")
	c := cors.New(cors.Options{
		AllowedOrigins: arrayAll,
		AllowedMethods: []string{
			http.MethodGet,
			http.MethodPost,
			http.MethodPut,
			http.MethodPatch,
			http.MethodDelete,
			http.MethodOptions,
			http.MethodHead,
		},
		AllowCredentials: true,
		AllowedHeaders:   []string{"*"},
	})
	return c.Handler(r)
}

func CheckRoutes() *mux.Router {
	r := mux.NewRouter()
	handlerInst := &handler.Handler{
		Service: &services.Service{
			Dal: &dal.Dal{},
		},
	}
	r.HandleFunc("/api/excel-report/v1/smart", handlerInst.GenerateExcelReport).Methods("GET")
	r.HandleFunc("/api/excel-report/v1/smart", handlerInst.GenerateExcelReport).Methods("POST")

	// live handlerInst

	lhandlerInst := &handler.LiveHandler{
		Service: &services.LiveService{
			Dal: &dal.LiveDal{},
		},
	}

	r.HandleFunc("/api/excel-report/v1/smart/live", lhandlerInst.TestExcelReportJson).Methods("GET")

	r.HandleFunc("/api/excel-report/v1/smart/live", lhandlerInst.TestExcelReportJson).Methods("POST")

	r.HandleFunc("/test/download", handler.TestFileDownload).Methods("POST")
	return r
}
