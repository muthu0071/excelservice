FROM golang:latest
RUN mkdir /app
ADD . /app
WORKDIR /app
CMD ["go get -v"]
RUN go build -o main
CMD ["/app/main"]
