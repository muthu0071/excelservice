package dal

import (
	"fmt"
	"nsdcexcelservice/db"
	"nsdcexcelservice/utils"
	"time"

	"gopkg.in/mgo.v2/bson"
)

func (l *LiveDal) GetAgencyPerfReportData(query bson.M, skipValue, limitValue int) ([]map[string]string, int, error) {

	fmt.Println("--agenchyPerfomance")
	delete(query, "reportName")
	dbInst, dbSees := db.GetMgoSession()
	defer dbSees.Close()
	reportData := []map[string]string{}
	dbSees.SetSocketTimeout(12 * time.Minute)
	tcount, _ := dbInst.C("trainingcentre").Find(nil).Count()
	fmt.Println("--checckcoutn", tcount)
	pipeQuery := queryForAgencyPerf(query, skipValue, limitValue)
	err := dbInst.C("trainingcentre").Pipe(pipeQuery).All(&reportData)

	if err != nil {
		return nil, 0, err
	}

	// fmt.Print("--lenreportData", len(reportData))
	return reportData, tcount, nil
}

func queryForAgencyPerf(query bson.M, skip, limit int) []bson.M {
	matchQuery := bson.M{}
	if len(query) != 0 {
		matchQuery = bson.M{
			"$match": query,
		}
	}
	projectQuery := bson.M{
		"$project": bson.M{
			"_id": 0,
			"tcId": bson.M{
				"$ifNull": []string{"$userName", ""},
			},
			"tcName": bson.M{
				"$ifNull": []string{"$trainingCentreName", ""},
			},

			"tcType": bson.M{
				"$ifNull": []string{"$type", ""},
			},

			"tpId": bson.M{
				"$ifNull": []string{"$trainingPartner.userName", ""},
			},

			"tpName": bson.M{
				"$ifNull": []string{"$trainingPartner.name", ""},
			},

			"tcAdress": bson.M{
				"$ifNull": []string{"$address.addressLine", ""},
			},

			"tcDist": bson.M{
				"$ifNull": []string{"$address.district.name", ""},
			},

			"tcStat": bson.M{
				"$ifNull": []string{"$address.state.name", ""},
			},

			"submissionDat": bson.M{
				"$ifNull": []string{"$submittedOn", ""},
			},

			"reInspctn": utils.IfNullQueyry("$canReInspect"),
			"daReviews": bson.M{
				"$arrayElemAt": []interface{}{"$daReviews", -1},
			},
			"withdrawal":              "",
			"actStatus":               utils.IfNullQueyry("$ActiveStatus"),
			"SSCReviwDate":            "",
			"totNumOfJobRol":          utils.IfNullQueyry("$counter.jobroles"),
			"totNmOfJobRolRecmdByQA":  "",
			"totNmOfJobRolRecmdBySSC": "",
			"qaStatus":                "",
			"sscStatus":               "",
			"qcReviews": bson.M{
				"$arrayElemAt": []interface{}{"$qcReviews", -1},
			},
			"jobRoles": bson.M{
				"$ifNull": []interface{}{"$jobRoles", ""},
			},
		},
	}

	tcworkflowLookUpQuery := bson.M{}
	tcworkflowLookUpQuery["$lookup"] = bson.M{
		"from": "tcworkflow",
		"let":  bson.M{"username": "$userName"},
	}

	tcworkflowLookUpQuerypipeline := []bson.M{}
	pipelineMatchQuery := bson.M{
		"$match": bson.M{"$expr": bson.M{"$and": []bson.M{
			bson.M{"$eq": []string{
				"$tcId", "$$username",
			}},
			bson.M{"$eq": []string{
				"$assignedNextUserRole", "Inspection Agency",
			}},
		}}},
	}
	pipelineSortQuery := bson.M{"$sort": bson.M{"_id": -1}}
	pipeLimit := bson.M{"$limit": 1}

	pipeLineProject := bson.M{
		"$project": bson.M{
			"insAgenNm":  "$tcworkflow[0].spoc.firstName",
			"insptnDate": "$tcworkflow[0].actionTakenOn",
		},
	}

	tcworkflowLookUpQuerypipeline = append(tcworkflowLookUpQuerypipeline, pipelineMatchQuery, pipelineSortQuery, pipeLimit, pipeLineProject)
	tcworkflowLookUpQuery["$lookup"].(bson.M)["pipeline"] = tcworkflowLookUpQuerypipeline
	tcworkflowLookUpQuery["$lookup"].(bson.M)["as"] = "tcworkFlow"

	unwindQuery := bson.M{
		"$unwind": bson.M{
			"path":                       "$tcWrkFlw",
			"preserveNullAndEmptyArrays": true,
		},
	}
	addFields := bson.M{
		"$addFields": bson.M{
			"desktopAssmntDate": bson.M{
				"$ifNull": []string{"$daReviews.date", ""},
			},
			"desktopAssmntStatus": bson.M{
				"$ifNull": []string{"$daReviews.review.status", ""},
			},
			"DAAgencyNm": bson.M{
				"$ifNull": []string{"$daReviews.userName", ""},
			},
			"insAgenNm": bson.M{
				"$ifNull": []string{"$tcWrkFlw.spoc.firstName", ""},
			},
			"insptnDate": bson.M{
				"$ifNull": []string{"$tcWrkFlw.actionTakenOn", ""},
			},
			"qcDate": bson.M{
				"$ifNull": []string{"$qcReviews.date", ""},
			},
		},
	}

	pipeQuery := []bson.M{}
	if len(matchQuery) != 0 {
		pipeQuery = append(pipeQuery, matchQuery)
	}

	if limit == 0 {
		pipeQuery = append(pipeQuery, bson.M{"$limit": 50000}, projectQuery, tcworkflowLookUpQuery, unwindQuery, addFields)
	} else {
		pipeQuery = append(pipeQuery, bson.M{"$skip": skip}, bson.M{"$limit": limit}, projectQuery, tcworkflowLookUpQuery, unwindQuery, addFields)
	}
	return pipeQuery
}
func GetCountOfagencyPerCollection() (int, error) {

	dbInst, dbSees := db.GetMgoSession()
	defer dbSees.Close()
	// reportData := []map[string]string{}
	dbSees.SetSocketTimeout(12 * time.Minute)
	return dbInst.C("trainingcentre").Find(nil).Count()
	// return 0, nil
}
