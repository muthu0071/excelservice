package dal

import (
	"fmt"
	"nsdcexcelservice/db"
	"nsdcexcelservice/utils"

	"gopkg.in/mgo.v2/bson"
)

// DalValue
func (l *LiveDal) GetPaymentDetailsData(query bson.M, skipValue, limitValue int) ([]map[string]string, int, error) {

	fmt.Println("--v1payment")
	lookUpQuery := bson.M{
		"$lookup": bson.M{
			"from":         "refunds",
			"localField":   "_id",
			"foreignField": "requestMetadata.paymentId",
			"as":           "refund",
		},
	}
	unwindQuery := bson.M{}
	unwindQuery["$unwind"] = bson.M{
		"path":                       "$refund",
		"preserveNullAndEmptyArrays": true,
	}

	projectQuery := bson.M{
		"$project": bson.M{
			"receiptPath":       1,
			"status":            "$responseMetaData.order_status",
			"paymentBy":         "$requestMetadata.subscriptionDetails.role",
			"orderId":           "$responseMetaData.order_id",
			"mercentId":         "$requestMetadata.merchant_id",
			"name":              "$requestMetadata.subscriptionDetails.name",
			"userName":          "$requestMetadata.referenceID",
			"tpId":              "$requestMetadata.dynamicDetails.tpUserName",
			"typeOfTc":          "$requestMetadata.dynamicDetails.tcType",
			"feeDescription":    "$requestMetadata.subscriptionDetails.referenceType",
			"sscName":           "$requestMetadata.subscriptionDetails.dynamicDetails.sscName",
			"qpCode":            "$requestMetadata.subscriptionDetails.dynamicDetails.qpcode",
			"applicationFees":   "$requestMetadata.amount",
			"jobRoleFees":       "$requestMetadata.subscriptionDetails.dynamicDetails.jobRoleFees",
			"conversationFees":  "$responseMetadata.trans_fee",
			"totalAmount":       "$amount",
			"transactiondate":   "$date",
			"transactionResult": "$responseMetadata.order_status",
			"address":           "$requestMetadata.subscriptionDetails.address.addressline",
			"refundDate":        "$refund.date",
			"referenceNo":       "$refund.responseMetadata.refund_ref_no",
			"refundstatus":      "$refund.responseMetadata.Refund_Order_Result.refund_status",
			"refundMode":        "NA",
			"refundBy":          "FinanceSpoc",
			"refundAmount":      "$refund.refund_amount",
			"remarks":           "NA",
		}}

	skipQuery := bson.M{
		"$skip": skipValue,
	}
	limitQuery := bson.M{
		"$limit": limitValue,
	}

	sortQuery := bson.M{}
	sortQuery["$sort"] = bson.M{"_id": -1}
	pipeQuery := []bson.M{}
	dbInst, dbSees := db.GetMgoSession()
	reportData := []map[string]string{}

	defer dbSees.Close()

	pipeQuery = append(pipeQuery, lookUpQuery, unwindQuery, projectQuery, sortQuery)
	countReportData := []map[string]string{}
	errCount := dbInst.C("payments").Pipe(pipeQuery).All(&countReportData)
	if limitValue == 0 {
		return countReportData, 0, nil
	}
	if errCount != nil {
		return nil, 0, errCount
	}

	totalCount := len(countReportData)

	// validate with Filter and paginatino
	pipeQuery = append(pipeQuery, skipQuery, limitQuery)

	err := dbInst.C("payments").Pipe(pipeQuery).All(&reportData)
	if err != nil {
		return nil, 0, err
	}
	// DalValue
	return reportData, totalCount, nil
}

func (l *LiveDal) GetPaymentDetailsDataForReport(query bson.M) ([]map[string]string, error) {
	_, transactionDatePresent := query["transactionfrom"]
	if transactionDatePresent {
		gteQuery := bson.M{
			"date": bson.M{"$gte": utils.ConvertStringIst(query["transactionfrom"].(string))}}
		lteQuery := bson.M{
			"date": bson.M{"$lte": utils.ConvertStringIst(query["transactionto"].(string))}}
		query["$and"] = []bson.M{gteQuery, lteQuery}
	}
	_, merchantIdPresent := query["merchantId"]
	if merchantIdPresent {
		query["requestMetadata.merchant_id"] = query["merchantId"]
	}
	delete(query, "reportName")
	delete(query, "transactionfrom")
	delete(query, "transactionto")
	delete(query, "merchantId")
	matchQuery := bson.M{}
	fmt.Println("--query", query)
	matchQuery["$match"] = query
	fmt.Println("--", len(matchQuery))
	lookUpQuery := bson.M{
		"$lookup": bson.M{
			"from":         "refunds",
			"localField":   "_id",
			"foreignField": "requestMetadata.paymentId",
			"as":           "refund",
		},
	}
	unwindQuery := bson.M{}
	unwindQuery["$unwind"] = bson.M{
		"path":                       "$refund",
		"preserveNullAndEmptyArrays": true,
	}

	projectQuery := bson.M{
		"$project": bson.M{
			"receiptPath":           bson.M{"$ifNull": []string{"$receiptPath", "NA"}},
			"status":                bson.M{"$ifNull": []string{"$responseMetaData.order_status", "NA"}},
			"paymentBy":             bson.M{"$ifNull": []string{"$requestMetadata.subscriptionDetails.role", "NA"}},
			"orderId":               bson.M{"$ifNull": []string{"$responseMetaData.order_id", "NA"}},
			"tranctnID":             bson.M{"$ifNull": []string{"$responseMetadata.tracking_id", "NA"}},
			"merchantId":            bson.M{"$ifNull": []string{"$requestMetadata.merchant_id", "NA"}},
			"invoiceNo":             bson.M{"$ifNull": []string{"$responseMetadata.tracking_id", "NA"}},
			"tpAplnNo":              "NA",
			"tcAplnNo":              "NA",
			"name":                  bson.M{"$ifNull": []string{"$requestMetadata.subscriptionDetails.name", "NA"}},
			"userName":              bson.M{"$ifNull": []string{"$requestMetadata.referenceID", "NA"}},
			"tpId":                  "$requestMetadata.dynamicDetails.tpUserName",
			"typeOfTc":              "$requestMetadata.dynamicDetails.tcType",
			"feeDescription":        "$requestMetadata.subscriptionDetails.referenceType",
			"sscName":               "$requestMetadata.subscriptionDetails.dynamicDetails.sscName",
			"qpCode":                "$requestMetadata.subscriptionDetails.dynamicDetails.qpcode",
			"applicationFees":       "$requestMetadata.amount",
			"jobRoleFees":           "$requestMetadata.subscriptionDetails.dynamicDetails.jobRoleFees",
			"conversationFees":      "$responseMetadata.trans_fee",
			"totalAmount":           "$amount",
			"transactiondate":       "$date",
			"transactiondateFormat": bson.M{"$dateToString": bson.M{"format": "%Y-%m-%d", "date": "$date"}},
			"transactionResult":     "$responseMetadata.order_status",
			"address":               "$requestMetadata.subscriptionDetails.address.addressline",
			"state":                 "$requestMetadata.subscriptionDetails.address.state",
			"district":              "$requestMetadata.subscriptionDetails.dynamicDetails.district",
			"paymentMode":           "NA",
			"refundDate":            "$refund.date",
			"referenceNo":           "$refund.responseMetadata.refund_ref_no",
			"refundstatus":          "$refund.responseMetadata.Refund_Order_Result.refund_status",
			"refundMode":            "NA",
			"refundBy":              "FinanceSpoc",
			"refundAmount":          bson.M{"$ifNull": []string{"$refund.refund_amount", "0"}},
			"remarks":               "NA",
			// "convertedRefuncAmount": bson.M{"$convert": bson.M{
			// "input": "$refund.refund_amount", "to": "string",
			// }},
		}}

	// skipQuery := bson.M{
	// 	"$skip": skipValue	,
	// }
	// limitQuery := bson.M{
	// 	"$limit": limitValue,
	// }

	sortQuery := bson.M{}
	sortQuery["$sort"] = bson.M{"_id": -1}
	pipeQuery := []bson.M{}
	dbInst, dbSees := db.GetMgoSession()
	// addtoFields := bson.M{"$addFields": bson.M{
	// 	"convertedRefuncAmount": bson.M{
	// 		"$convert": bson.M{
	// 			"input": "$refundAmount", "to": "string",
	// 		},
	// 	},
	// }}
	defer dbSees.Close()
	if len(query) != 0 {
		pipeQuery = append(pipeQuery, matchQuery, lookUpQuery, unwindQuery, projectQuery, sortQuery)
	} else {
		fmt.Println("--elseQuery----")
		pipeQuery = append(pipeQuery, lookUpQuery, unwindQuery, projectQuery, sortQuery)

	}
	countReportData := []map[string]string{}
	errCount := dbInst.C("payments").Pipe(pipeQuery).All(&countReportData)
	if errCount != nil {
		return nil, errCount
	}
	// fmt.Println("--countdata", countReportData[0]["convertedRefuncAmount"])

	return countReportData, errCount
}
