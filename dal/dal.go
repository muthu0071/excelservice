package dal

import (
	"fmt"
	"nsdcexcelservice/constants"
	"nsdcexcelservice/db"

	"gopkg.in/mgo.v2/bson"
)

type Dal struct {
}
type IDal interface {
	GetReportData(findQuery bson.M) (reportData []map[string]string, err error)
	FindReportAsJson(findQuery bson.M, skipValue, limitValue int) (reportData []map[string]string, count int, err error)
	GetReportLiveData(findQuery bson.M, skipValue, limitValue int) (reportData []map[string]string, count int, err error)
}

func (d *Dal) GetReportData(findQuery bson.M) (reportData []map[string]string, err error) {
	dbInst, dbSees := db.GetMgoSession()
	defer dbSees.Close()
	fmt.Println("--findquery", findQuery)
	err = dbInst.C(constants.ColSmartReports).Find(findQuery).Select(bson.M{"_id": 0}).All(&reportData)
	return
}
func (d *Dal) FindReportAsJson(findQuery bson.M, skipValue, limitValue int) (reportData []map[string]string, count int, err error) {
	dbInst, dbSees := db.GetMgoSession()
	defer dbSees.Close()
	matchQuery := bson.M{"$match": findQuery}
	skipQuery := bson.M{"$skip": skipValue}
	limitQuery := bson.M{"$limit": limitValue}
	projectQuery := bson.M{"$project": bson.M{"_id": 0}}
	pipeQuery := []bson.M{}
	pipeQuery = append(pipeQuery, matchQuery, skipQuery, limitQuery, projectQuery)
	fmt.Println("--checkValue", skipValue, limitValue)
	count, _ = dbInst.C(constants.ColSmartReports).Find(findQuery).Count()

	if count < 10 {
		skipValue = 0
	}
	
	fmt.Println("--query", findQuery)

	// err = dbInst.C(constants.ColSmartReports).Pipe(pipeQuery).All(&reportData)
	err = dbInst.C(constants.ColSmartReports).Find(findQuery).Skip(skipValue).Limit(limitValue).Select(bson.M{"_id": 0}).All(&reportData)
	return
}

func (d *Dal) GetReportLiveData(findQuery bson.M, skipValue, limitValue int) (reportData []map[string]string, count int, err error) {

	 
	return FindPaymentDetails(findQuery, skipValue, limitValue)

}

func FindPaymentDetails(query bson.M, skipValue, limitValue int) ([]map[string]string, int, error) {
	lookUpQuery := bson.M{
		"$lookup": bson.M{
			"from":         "refunds",
			"localField":   "_id",
			"foreignField": "requestMetadata.paymentId",
			"as":           "refund",
		},
	}
	unwindQuery := bson.M{
		"$unwind": "$refund",
	}
	projectQuery := bson.M{
		"$project": bson.M{
			"receiptPath":       1,
			"status":            "$responseMetaData.order_status",
			"paymentBy":         "$requestMetadata.subscriptionDetails.role",
			"orderId":           "$responseMetaData.order_id",
			"mercentId":         "$requestMetadata.merchant_id",
			"name":              "$requestMetadata.subscriptionDetails.name",
			"userName":          "$requestMetadata.referenceID",
			"tpId":              "$requestMetadata.dynamicDetails.tpUserName",
			"typeOfTc":          "$requestMetadata.dynamicDetails.tcType",
			"feeDescription":    "$requestMetadata.subscriptionDetails.referenceType",
			"sscName":           "$requestMetadata.subscriptionDetails.dynamicDetails.sscName",
			"qpCode":            "$requestMetadata.subscriptionDetails.dynamicDetails.qpcode",
			"applicationFees":   "$requestMetadata.amount",
			"jobRoleFees":       "$requestMetadata.subscriptionDetails.dynamicDetails.jobRoleFees",
			"conversationFees":  "$responseMetadata.trans_fee",
			"totalAmount":       "$amount",
			"transactiondate":   "$date",
			"transactionResult": "$responseMetadata.order_status",
			"address":           "$requestMetadata.subscriptionDetails.address.addressline",
			"refundDate":        "$refund.date",
			"referenceNo":       "$refund.responseMetadata.refund_ref_no",
			"refundstatus":      "$refund.responseMetadata.Refund_Order_Result.refund_status",
			"refundMode":        "NA",
			"refundBy":          "FinanceSpoc",
			"refundAmount":      "$refund.refund_amount",
			"remarks":           "NA",
		}}

	skipQuery := bson.M{
		"$skip": skipValue,
	}
	limitQuery := bson.M{
		"$limit": limitValue,
	}

	sortQuery := bson.M{}
	sortQuery["$sort"] = bson.M{"_id": -1}
	pipeQuery := []bson.M{}
	dbInst, dbSees := db.GetMgoSession()
	reportData := []map[string]string{}

	fmt.Println(projectQuery, skipQuery, limitQuery, unwindQuery)
	pipeQuery = append(pipeQuery, lookUpQuery, projectQuery, skipQuery, sortQuery)
	defer dbSees.Close()
	err := dbInst.C("payments").Pipe(pipeQuery).All(&reportData)

	if err != nil {
		return nil, 0, err
	}
	return reportData, 0, nil
}
