package dal

import (
	"fmt"
	"nsdcexcelservice/constants"
	"nsdcexcelservice/db"
	"nsdcexcelservice/utils"

	"gopkg.in/mgo.v2/bson"
)

type LiveDal struct {
}

type ILiveDal interface {
	GetPaymentDetailsData(query bson.M, skipValue, limitValue int) ([]map[string]string, int, error)
	GetReportData(query bson.M) map[string]func(bson.M) ([]map[string]string, error)
	GetPaymentDetailsDataForReport(query bson.M) ([]map[string]string, error)
	GetReportJsonData() map[string]func(bson.M, int, int) ([]map[string]string, int, error)
}

func (l *LiveDal) GetReportData(query bson.M) map[string]func(bson.M) ([]map[string]string, error) {

	dalValue := map[string]func(bson.M) ([]map[string]string, error){}
	dalValue[constants.PaymentDetailsReport] = l.GetPaymentDetailsDataForReport
	return dalValue
}

func (l *LiveDal) GetReportJsonData() map[string]func(bson.M, int, int) ([]map[string]string, int, error) {

	jsonDal := map[string]func(bson.M, int, int) ([]map[string]string, int, error){}
	fmt.Print("--report json Data")
	jsonDal[constants.PaymentDetailsReport] = l.GetPaymentDetailsDatav1
	jsonDal[constants.AgencyPerformanceReport] = l.GetAgencyPerfReportData
	jsonDal[constants.TcRefundReport] = l.GetTcRefundDetailsData
	return jsonDal
}

func (l *LiveDal) GetReportJsonDataChan() map[string]func(bson.M, int, int) ([]map[string]string, int, error) {

	jsonDal := map[string]func(bson.M, int, int) ([]map[string]string, int, error){}
	fmt.Print("--report json Data")
	jsonDal[constants.PaymentDetailsReport] = l.GetPaymentDetailsDatav1
	jsonDal[constants.AgencyPerformanceReport] = l.GetAgencyPerfReportData
	return jsonDal
}

func (l *LiveDal) GetPaymentDetailsDatav1(query bson.M, skipValue, limitValue int) ([]map[string]string, int, error) {
	_, transactionDatePresent := query["transactionfrom"]
	fmt.Println("--paymen tdetasv1")
	if transactionDatePresent {
		gteQuery := bson.M{
			"date": bson.M{"$gte": utils.ConvertStringIst(query["transactionfrom"].(string))}}
		lteQuery := bson.M{
			"date": bson.M{"$lte": utils.ConvertStringIst(query["transactionto"].(string))}}
		query["$and"] = []bson.M{gteQuery, lteQuery}
	}
	_, merchantIdPresent := query["merchantId"]
	if merchantIdPresent {
		query["requestMetadata.merchant_id"] = query["merchantId"]
	}
	delete(query, "reportName")
	delete(query, "transactionfrom")
	delete(query, "transactionto")
	delete(query, "merchantId")
	matchQuery := bson.M{}
	fmt.Println("--query", query)
	matchQuery["$match"] = query
	fmt.Println("--", len(matchQuery))
	lookUpQuery := bson.M{
		"$lookup": bson.M{
			"from":         "refunds",
			"localField":   "_id",
			"foreignField": "requestMetadata.paymentId",
			"as":           "refund",
		},
	}
	unwindQuery := bson.M{}
	unwindQuery["$unwind"] = bson.M{
		"path":                       "$refund",
		"preserveNullAndEmptyArrays": true,
	}

	projectQuery := bson.M{
		"$project": bson.M{
			"receiptPath":           bson.M{"$ifNull": []string{"$receiptPath", "NA"}},
			"status":                bson.M{"$ifNull": []string{"$responseMetaData.order_status", "Not Completed"}},
			"paymentBy":             bson.M{"$ifNull": []string{"$requestMetadata.subscriptionDetails.role", "NA"}},
			"orderId":               bson.M{"$ifNull": []string{"$sequenceId", "NA"}},
			"tranctnID":             bson.M{"$ifNull": []string{"$responseMetadata.tracking_id", "NA"}},
			"merchantId":            bson.M{"$ifNull": []string{"$requestMetadata.merchant_id", "NA"}},
			"invoiceNo":             bson.M{"$ifNull": []string{"$responseMetadata.invoice_no", "NA"}},
			"tpAplnNo":              "NA",
			"tcAplnNo":              "NA",
			"name":                  bson.M{"$ifNull": []string{"$requestMetadata.subscriptionDetails.name", "NA"}},
			"userName":              bson.M{"$ifNull": []string{"$requestMetadata.subscriptionDetails.referenceID", "NA"}},
			"tpId":                  bson.M{"$ifNull": []string{"$requestMetadata.subscriptionDetails.dynamicDetails.tpUserName", "NA"}},
			"typeOfTc":              bson.M{"$ifNull": []string{"$requestMetadata.subscriptionDetails.dynamicDetails.tcType", "NA"}},
			"feeDescription":        bson.M{"$ifNull": []string{"$requestMetadata.subscriptionDetails.referenceType", "NA"}},
			"sscName":               bson.M{"$ifNull": []string{"$requestMetadata.subscriptionDetails.dynamicDetails.sscName", "NA"}},
			"qpCode":                bson.M{"$ifNull": []string{"$requestMetadata.subscriptionDetails.dynamicDetails.qpcode", "NA"}},
			"applicationFees":       bson.M{"$ifNull": []string{"$requestMetadata.amount", "NA"}},
			"jobRoleFees":           bson.M{"$ifNull": []string{"$requestMetadata.subscriptionDetails.dynamicDetails.jobRoleFees", "NA"}},
			"conversationFees":      bson.M{"$ifNull": []string{"$responseMetadata.trans_fee", "NA"}},
			"totalAmount":           bson.M{"$ifNull": []string{"$amount", "NA"}},
			"transactiondate":       "$date",
			"transactiondateFormat": bson.M{"$dateToString": bson.M{"format": "%Y-%m-%d", "date": "$date"}},
			"transactionResult":     bson.M{"$ifNull": []string{"$responseMetadata.order_status", "NA"}},
			"address":               bson.M{"$ifNull": []string{"$requestMetadata.subscriptionDetails.address.addressline", "NA"}},
			"state":                 bson.M{"$ifNull": []string{"$requestMetadata.subscriptionDetails.address.state", "NA"}},
			"district":              bson.M{"$ifNull": []string{"$requestMetadata.subscriptionDetails.dynamicDetails.district", "NA"}},
			"paymentMode":           "NA",
			"refundDate":            bson.M{"$ifNull": []string{"$refund.date", "NA"}},
			"referenceNo":           bson.M{"$ifNull": []string{"$refund.responseMetadata.refund_ref_no", "NA"}},
			"refundstatus":          bson.M{"$ifNull": []string{"$refund.responseMetadata.Refund_Order_Result.refund_status", "NA"}},
			"refundMode":            "NA",
			"refundBy":              "FinanceSpoc",
			"refundAmount":          bson.M{"$ifNull": []string{"$refund.refund_amount", "0"}},
			"remarks":               "NA",
		}}

	skipQuery := bson.M{
		"$skip": skipValue,
	}
	limitQuery := bson.M{
		"$limit": limitValue,
	}

	sortQuery := bson.M{}
	sortQuery["$sort"] = bson.M{"_id": -1}
	pipeQuery := []bson.M{}
	dbInst, dbSees := db.GetMgoSession()
	reportData := []map[string]string{}

	defer dbSees.Close()
	if len(query) != 0 {
		pipeQuery = append(pipeQuery, matchQuery, lookUpQuery, unwindQuery, projectQuery, sortQuery)
	} else {
		fmt.Println("--elseQuery----")
		pipeQuery = append(pipeQuery, lookUpQuery, unwindQuery, projectQuery, sortQuery)

	}

	// pipeQuery = append(pipeQuery, lookUpQuery, unwindQuery, projectQuery, sortQuery)
	countReportData := []map[string]string{}
	errCount := dbInst.C("payments").Pipe(pipeQuery).All(&countReportData)
	if limitValue == 0 {
		return countReportData, 0, nil
	}
	if errCount != nil {
		return nil, 0, errCount
	}

	totalCount := len(countReportData)
	fmt.Println("ksip", skipQuery, limitQuery)
	pipeQuery = append(pipeQuery, skipQuery, limitQuery)
	// } else {
	// 	fmt.Println("--elseQuery----")
	// 	pipeQuery = append(pipeQuery, lookUpQuery, unwindQuery, projectQuery, sortQuery)

	// }

	// // validate with Filter and paginatino
	// pipeQuery = append(pipeQuery, skipQuery, limitQuery)

	err := dbInst.C("payments").Pipe(pipeQuery).All(&reportData)
	if err != nil {
		return nil, 0, err
	}
	// DalValue
	return reportData, totalCount, nil
}

func (l *LiveDal) GetTcRefundDetailsData(query bson.M, skipValue, limitValue int) ([]map[string]string, int, error) {

	pipeQuery := []bson.M{}
	reportData := []map[string]string{}
	dbInst, dbSees := db.GetMgoSession()
	defer dbSees.Close()
	totalCount := 0
	fmt.Println("--getget TCrefundDetails")
	matchQuery := bson.M{"$match": bson.M{
		// "requestMetadata.paymentId": bson.M{"$exists": true},
	}}

	projectQuery := bson.M{
		"$project": bson.M{"_id": 0,
			"merchantId": bson.M{
				"$ifNull": []string{"$requestMetadata.merchant_id", "NA"},
			},
			"trxnID": bson.M{
				"$ifNull": []string{"$requestMetadata.reference_no", ""},
			},
			"userID": bson.M{
				"$ifNull": []string{"$userId", "NA"},
			},
			"refndAmnt": bson.M{
				"$ifNull": []string{"$requestMetadata.refund_amount", "NA"},
			},
			"feeType": bson.M{
				"$ifNull": []string{"$requestMetadata.subscriptionDetails.referenceType", "NA"},
			},
			"trxnDate": bson.M{
				"$ifNull": []string{"$date", "NA"},
			},
			"trxnResult": bson.M{
				"$ifNull": []string{"$responseMetadata.refund_request", ""},
			},
		},
	}

	lookUpQuery := bson.M{
		"$lookup": bson.M{
			"from":         "trainingcentre",
			"localField":   "userID",
			"foreignField": "userName",
			"as":           "tcData",
		},
	}
	unwindQuery := bson.M{
		"$unwind": "$tcData",
	}
	addFieldsValue := bson.M{
		"$addFields": bson.M{
			"tpName": bson.M{
				"$ifNull": []string{"$tcData.trainingPartner.name", "NA"},
			},
			"tpID": bson.M{
				"$ifNull": []string{"$tcData.trainingPartner.userName", "NA"},
			},
			"tcID": bson.M{
				"$ifNull": []string{"$tcData.userName", "NA"},
			},
			"tcName": bson.M{
				"$ifNull": []string{"$tcData.trainingCentreName", "na"},
			},
			"tcType": bson.M{
				"$ifNull": []string{"$tcData.trainingCenterType", ""},
			},
			"tcStatus": bson.M{
				"$ifNull": []string{"$tcData.status", ""},
			},
		}}
	project2 := bson.M{
		"$project": bson.M{
			"tcData": 0,
			"userID": 0,
		},
	}

	skipQuery := bson.M{
		"$skip": skipValue,
	}
	limitQuery := bson.M{
		"$limit": limitValue,
	}

	sortQuery := bson.M{}
	sortQuery["$sort"] = bson.M{"_id": -1}
	if limitValue != 0 {
		pipeQuery = append(pipeQuery, sortQuery, skipQuery, limitQuery, matchQuery, projectQuery, lookUpQuery, unwindQuery, addFieldsValue, project2)
	} else {
		pipeQuery = append(pipeQuery, sortQuery, matchQuery, projectQuery, lookUpQuery, unwindQuery, addFieldsValue, project2)

	}

	err := dbInst.C("refunds").Pipe(pipeQuery).All(&reportData)
	if err != nil {
		return nil, 0, err
	}
	return reportData, totalCount, nil
}
