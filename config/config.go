package config

import (
	"os"

	"github.com/BurntSushi/toml"
)

var Data map[string]string

func GetEnv(value string) string {

	return GetTomlFile()[value]
}

func GetTomlFile() map[string]string {
	wd, _ := os.Getwd()
	_, err := toml.DecodeFile(wd+"/config/app.toml", &Data)
	if err != nil {
		panic(err)
	}
	return Data
}
