package constants

const (
	RecomendataionsReport   = "recomendationReport"
	TcStatusReports         = "tcStatusReport"
	TargetAllocationReport  = "targetAllocationReport"
	ContinuousReport        = "continuousReport"
	TpReport                = "tpReport"
	AgencyPerformanceReport = "agencyPerformanceReport"
	TcRefundReport          = "tcRefunddReport"
	PaymentDetailsReport    = "paymentDtlsReport"
	//-----
	TPTCStatusReport = "TPTCStatusReport"
)

// collection Names
const (
	ColSmartReports = "smartreports"
)
